#! /bin/bash

xhost +; \
docker run --privileged --rm --network host -it \
  --volume "$(pwd)"/:/slungt/ \
  --volume /tmp/.X11-unix:/tmp/.X11-unix \
  --env DISPLAY --env QT_X11_NO_MITSHM=1 \
  slungt
