FROM docker.io/ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

RUN apt-get update && apt-get upgrade -y
RUN apt-get update && apt-get install -y --no-install-recommends wget curl nano git

# Install python
RUN apt-get update && apt-get install -y --no-install-recommends python3 python3-pip python3-dev
RUN pip3 install --upgrade --no-cache-dir pip
RUN python3 -V && pip3 --version

# Install swig
RUN apt-get update && apt-get install -y --no-install-recommends build-essential
RUN apt-get update && apt-get install -y --no-install-recommends swig

# Install kenlm
RUN apt-get update && apt-get install -y --no-install-recommends \
 cmake libboost-system-dev libboost-thread-dev libboost-program-options-dev \
 libboost-test-dev libeigen3-dev zlib1g-dev libbz2-dev liblzma-dev
RUN git clone --depth 1 https://github.com/kpu/kenlm.git
RUN cd /kenlm/; mkdir -p build/
RUN cd /kenlm/build/; cmake ..
RUN cd /kenlm/build/; make -j 4
RUN pip3 install --no-cache https://github.com/kpu/kenlm/archive/master.zip

# Slungt dependencies
RUN pip3 install --upgrade --no-cache-dir numpy tqdm

WORKDIR /
CMD ["/bin/bash"]
