%module slungt
%{
    // Includes the header in the wrapper code
    #include "../slungt/beam_result.hpp"
    #include "../slungt/intent.hpp"
    #include "../slungt/interface.hpp"
    #include "../slungt/trie_state.hpp"
    #include "../slungt/trie.hpp"
%}

// Some modules need extra imports beside the main .hpp file
%include "std_string.i"
%include "std_vector.i"

// Instantiate templates used by example
namespace std {
    %template(VectorString) vector<string>;
    %template(VectorVectorString) vector<vector<string>>;
    %template(VectorFloat) vector<float>;
    %template(VectorVectorFloat) vector<vector<float>>;
    %template(VectorBeamresult) vector<BeamResult>;
    %template(VectorEntity) vector<Entity>;
}

// Convert vector to native (python) list
%naturalvar BeamResult::tokens;
%naturalvar Intent::entities;

// Improve printing of result objects
%extend BeamResult {
    std::string __str__() const {
         return $self->to_string();
    }
}
%extend Entity {
    std::string __str__() const {
         return $self->to_string();
    }
}
%extend Intent {
    std::string __str__() const {
         return $self->to_string();
    }
}

// Ignore: Warning 503: Can't wrap 'operator <<' unless renamed to a valid identifier.
%warnfilter(503) BeamResult;
%warnfilter(503) Entity;
%warnfilter(503) Intent;

// Ignore: Warning 511: Can't use keyword arguments with overloaded functions.
// The warning is cause by enabling keyword arguments, which doesn't work for vectors.
#pragma SWIG nowarn=511

// Parse the header file to generate wrappers
%include "../slungt/beam_result.hpp"
%include "../slungt/intent.hpp"
%include "../slungt/interface.hpp"
%include "../slungt/trie_state.hpp"
%include "../slungt/trie.hpp"
