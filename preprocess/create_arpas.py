import argparse
import os
import subprocess

# ==================================================================================================

input_dirpath = "sents/"
output_dirpath = "arpas/"

create_cmd = (
    "/kenlm/build/bin/lmplz --order 5 --temp_prefix /tmp/ --memory 95% "
    + "--discount_fallback --text {} --arpa {}"
)

compress_cmd = "/kenlm/build/bin/build_binary -a 255 -q 8 -v trie {} {}"

# ==================================================================================================


def create_arpas(workdir: str) -> None:
    output_dir = os.path.join(workdir, output_dirpath)
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    sdir = os.path.join(workdir, input_dirpath)
    adir = os.path.join(workdir, output_dirpath)

    for tfile in os.listdir(sdir):
        print("")
        tpath = os.path.join(sdir, tfile)
        apath = os.path.join(adir, tfile.replace(".txt", ".arpa"))
        bpath = apath.replace(".arpa", ".bin")

        cmd = create_cmd.format(tpath, apath)
        print("Calling:", "'{}'".format(cmd))
        subprocess.call(["/bin/bash", "-c", cmd])

        cmd = compress_cmd.format(apath, bpath)
        print("Calling:", "'{}'".format(cmd))
        subprocess.call(["/bin/bash", "-c", cmd])


# ==================================================================================================


def main() -> None:
    parser = argparse.ArgumentParser(description="")
    parser.add_argument(
        "--workdir",
        help="Path of the working directory",
        type=str,
        required=True,
    )
    args = parser.parse_args()

    create_arpas(args.workdir)


# ==================================================================================================

if __name__ == "__main__":
    main()
