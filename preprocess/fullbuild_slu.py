import argparse
import os
import shutil
import time

import build_sentences
import collect_words
import create_arpas
import map_intent_slots

# ==================================================================================================


def emtpy_with_ignore(path: str) -> None:
    """Deletes and recreates a directory. Adds a gitignore file to ignore directory contents"""

    if os.path.isdir(path):
        try:
            shutil.rmtree(path, ignore_errors=True)
            os.mkdir(path)
        except OSError as err:
            if len(os.listdir(path)) == 0:
                # In the special case that this directory is mounted as volume it can't be deleted
                # But it will be emptied so the error can be ignored
                pass
            else:
                raise err
    else:
        os.mkdir(path)

    with open(path + ".gitignore", "w+", encoding="utf-8") as file:
        file.write("*\n!.gitignore\n")


# ==================================================================================================


def fullbuild(workdir: str, nlufile_path: str, alphabet_path: str) -> None:
    print("")

    # Delete directory contents, else leftover FSTs might cause errors while merging
    emtpy_with_ignore(workdir)

    map_intent_slots.build_map(workdir, nlufile_path)
    collect_words.create_symbols(workdir, nlufile_path, alphabet_path)
    build_sentences.build_ngram_sentences(workdir, nlufile_path)
    create_arpas.create_arpas(workdir)


# ==================================================================================================


def main() -> None:
    parser = argparse.ArgumentParser(description="")
    parser.add_argument(
        "--workdir",
        help="Path of the working directory",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--nlufile_path",
        help="Path to a json file with sample intents",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--alphabet_path",
        help="Path to the alphabet.json file",
        type=str,
        required=True,
    )
    args = parser.parse_args()

    start = time.time()
    fullbuild(args.workdir, args.nlufile_path, args.alphabet_path)
    finish = time.time()

    print("Building the LMs took {:.3f} seconds".format(finish - start))


# ==================================================================================================

if __name__ == "__main__":
    main()
