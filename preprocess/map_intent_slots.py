import argparse
import json
import os
import re
from typing import Any, Dict

# ==================================================================================================

output_dirpath = "slots/"

# ==================================================================================================


def find_slots(nlufile_path: str, output_dir: str) -> None:
    print("Searching slots in intents ...")

    with open(nlufile_path, "r", encoding="utf-8") as file:
        nludata = json.load(file)

    map_is: Dict[str, Any[str]] = {}
    for intent in nludata["intents"]:
        map_is[intent] = set()

        for line in nludata["intents"][intent]:
            matches = re.findall(r"]\(([a-zA-Z0-9?\-_]+)\)", line)
            if len(matches) > 0:
                map_is[intent] = map_is[intent].union(set(matches))

    for intent in list(map_is.keys()):
        map_is[intent] = sorted(list(map_is[intent]))

    # Switch direction
    map_si: dict = {}
    for k, v in map_is.items():
        for x in v:
            map_si.setdefault(x, []).append(k)

    # Save maps to file
    outpath = os.path.join(output_dir, "intent_slot_map.json")
    with open(outpath, "w+", encoding="utf-8") as file:
        json.dump(map_is, file, indent=2)

    outpath = os.path.join(output_dir, "slot_intent_map.json")
    with open(outpath, "w+", encoding="utf-8") as file:
        json.dump(map_si, file, indent=2)


# ==================================================================================================


def build_map(workdir: str, nlufile_path: str) -> None:
    output_dir = os.path.join(workdir, output_dirpath)
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    find_slots(nlufile_path, output_dir)


# ==================================================================================================


def main() -> None:
    parser = argparse.ArgumentParser(description="")
    parser.add_argument(
        "--workdir",
        help="Path of the working directory",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--nlufile_path",
        help="Path to a json file with sample intents",
        type=str,
        required=True,
    )
    args = parser.parse_args()

    build_map(args.workdir, args.nlufile_path)


# ==================================================================================================

if __name__ == "__main__":
    main()
