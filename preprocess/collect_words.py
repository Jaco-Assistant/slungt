import argparse
import json
import os
import re
from typing import Dict, List, Set, Tuple

import tqdm

# ==================================================================================================

slotmap_path = "slots/slot_intent_map.json"
output_dirpath = "vocab/"

# ==================================================================================================


def load_alphabet(alphabet_path: str) -> Tuple[List[str], List[str]]:
    with open(alphabet_path, "r", encoding="utf-8") as file:
        alphabet: List[str] = json.load(file)

    # Replace special symbols
    alphabet = [a.replace("<unk>", " ") for a in alphabet]
    alphabet = [a.replace("▁", " ") for a in alphabet]
    alphabet = sorted(set(alphabet))

    # Collect unique chars in the alphabet
    acs_list = [set(a) for a in alphabet]
    alphabet_chars = sorted(set().union(*acs_list))

    return alphabet, alphabet_chars


# ==================================================================================================


def collect_words_txt(sentences_path: str) -> Set[str]:
    """Handles a text file with one sentence per line"""

    with open(sentences_path, "r", encoding="utf-8") as file:
        lines = file.readlines()

    unique_words: Set[str] = set()
    for line in tqdm.tqdm(lines):
        line_lower = line.lower()
        words = [w.strip() for w in line_lower.split()]
        unique_words = unique_words.union(set(words))

    return unique_words


# ==================================================================================================


def intentsample2components(sample: str) -> List[str]:
    splitter = r" (?![^(]*\))(?![^[]*\])"
    components = re.split(splitter, sample)
    return components


# ==================================================================================================


def collect_words_json(sentences_path: str, intentmap_path: str) -> Dict[str, Set[str]]:
    """Handles a nlu file in Jaco's format"""

    print("Collecting words from intents and lookups ...")

    with open(sentences_path, "r", encoding="utf-8") as file:
        nludata = json.load(file)

    with open(intentmap_path, "r", encoding="utf-8") as file:
        slotmap = json.load(file)

    intent_words: Dict[str, Set[str]] = {}

    total_samples = sum((len(nludata["intents"][e]) for e in nludata["intents"]))
    with tqdm.tqdm(total=total_samples) as pbar:
        # Words in intents
        for intent in nludata["intents"]:
            unique_words: set = set()

            for line in nludata["intents"][intent]:
                components = intentsample2components(line)
                for component in components:
                    if component.startswith("("):
                        # Split alternatives
                        component = re.sub(r"[\(\|\)]", " ", component)
                        words = [w.strip() for w in component.split(" ")]
                        unique_words = unique_words.union(set(words))

                    else:
                        # Simple word
                        unique_words = unique_words.union({component})

                pbar.update(1)
            intent_words[intent] = unique_words

    total_samples = sum((len(nludata["lookups"][e]) for e in nludata["lookups"]))
    with tqdm.tqdm(total=total_samples) as pbar:
        # Words in lookups
        for lookup in nludata["lookups"]:
            unique_words = set()

            roles = []
            for lsm in slotmap:
                if lsm.startswith(lookup):
                    if "?" in lsm:
                        role = "?" + lsm.split("?")[1]
                    else:
                        role = ""
                    roles.append(role)

            for line in nludata["lookups"][lookup]:
                pbar.update(1)

                if len(roles) == 0:
                    continue

                if "->" in line:
                    # Split up alternative options in synonyms
                    prefix, suffix = line.split("->")
                    prefix = prefix.replace("(", "").replace(")", "")
                    prefixes = prefix.split("|")
                    for pf in prefixes:
                        for role in roles:
                            word = "[" + pf + "->" + suffix + "](" + lookup + role + ")"
                            unique_words.add(word)
                else:
                    for role in roles:
                        word = "[" + line + "](" + lookup + role + ")"
                        unique_words.add(word)

            if len(roles) > 0:
                for lsm, intents in slotmap.items():
                    if lsm.startswith(lookup):
                        for intent in intents:
                            intent_words[intent] = intent_words[intent].union(
                                unique_words
                            )

    return intent_words


# ==================================================================================================


def filter_words(
    unique_words_sorted: List[str], alphabet_chars: List[str]
) -> Tuple[List[str], List[str]]:

    unique_words_filtered = []
    bad_words = []
    for word in unique_words_sorted:
        if not set(word).issubset(alphabet_chars):
            bad_words.append(word)
        elif word in [" ", "'"]:
            bad_words.append(word)
        else:
            unique_words_filtered.append(word)

    return unique_words_filtered, bad_words


# ==================================================================================================


def collect_words(
    sentences_path: str, alphabet_path: str, workdir: str, output_dir: str
) -> None:
    # Collect unique words
    if sentences_path.endswith(".txt"):
        unique_words = collect_words_txt(sentences_path)
        unique_words.discard("")
        print("\nFound {} unique words".format(len(unique_words)))
        unique_words_sorted = sorted(unique_words)

    elif sentences_path.endswith(".json"):
        intentmap_path = os.path.join(workdir, slotmap_path)
        intent_words = collect_words_json(sentences_path, intentmap_path)
        unique_words = set()
        intent_words_sorted = {}
        for intent in intent_words:
            intent_words[intent].discard("")
            intent_words_sorted[intent] = sorted(intent_words[intent])
            unique_words = unique_words.union(intent_words[intent])
        print("\nFound {} unique words".format(len(unique_words)))

    else:
        raise ValueError(
            "Sentence format has to be either .txt or .json, got something else"
        )

    # Filter words with characters not in the alphabet
    _, alphabet_chars = load_alphabet(alphabet_path)
    alphabet_chars.extend(["[", "]", "(", ")", "-", ">", "_", "?"])
    alphabet_chars.extend([str(i) for i in range(10)])
    bad_words: List[str] = []

    if sentences_path.endswith(".txt"):
        unique_words_filtered, bad_words = filter_words(
            unique_words_sorted, alphabet_chars
        )
    elif sentences_path.endswith(".json"):
        for intent in intent_words:
            unique_words_filtered, bws = filter_words(
                intent_words_sorted[intent], alphabet_chars
            )
            intent_words_sorted[intent] = unique_words_filtered
            bad_words.extend(bws)
        bad_words = sorted(list(set(bad_words)))

    if len(bad_words) > 0:
        print("WARNING: Excluded following words: ", bad_words)

    # Save collections
    if sentences_path.endswith(".txt"):
        output_file = os.path.join(output_dir, os.path.basename(sentences_path))
        text = "\n".join(unique_words_filtered) + "\n"
        with open(output_file, "w+", encoding="utf-8") as file:
            file.write(text)

    elif sentences_path.endswith(".json"):
        for intent in intent_words:
            output_file = os.path.join(output_dir, intent + ".txt")
            text = "\n".join(intent_words_sorted[intent]) + "\n"
            with open(output_file, "w+", encoding="utf-8") as file:
                file.write(text)


# ==================================================================================================


def create_symbols(workdir: str, sentences_path: str, alphabet_path: str) -> None:
    output_dir = os.path.join(workdir, output_dirpath)
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    collect_words(sentences_path, alphabet_path, workdir, output_dir)


# ==================================================================================================


def main() -> None:
    parser = argparse.ArgumentParser(description="")
    parser.add_argument(
        "--sentences_path",
        help="Path to a text file with sample sentences or a json file with sample intents",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--alphabet_path",
        help="Path to the alphabet.json file",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--workdir",
        help="Path of the working directory",
        type=str,
        required=True,
    )
    args = parser.parse_args()

    create_symbols(args.workdir, args.sentences_path, args.alphabet_path)


# ==================================================================================================

if __name__ == "__main__":
    main()
