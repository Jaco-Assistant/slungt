import kenlm

lm_path = "/kenlm/lm/test.arpa"
model = kenlm.LanguageModel(lm_path)

state = kenlm.State()
model.BeginSentenceWrite(state)

words = ["language", "modeling", "is", "fun", "</s>"]
scores = []
for word in words:
    state2 = kenlm.State()
    score = model.BaseScore(state, word, state2)
    state = state2
    print(word, score)
    scores.append(score)

wscore = sum(scores)
mscore = model.score("language modeling is fun")
assert abs(wscore - mscore) < 1e-3
print("total:", mscore)
print("--finished--")
