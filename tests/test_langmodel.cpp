#include <iostream>
#include <string>
#include <math.h>
#include <vector>

#include "../slungt/langmodel.hpp"

// =================================================================================================

void test_scoring()
{
    std::string lm_path = "/kenlm/lm/test.arpa";
    bool success = true;

    std::unique_ptr<Langmodel> model = std::make_unique<Langmodel>(lm_path, 1.0, 0.0, true);
    std::shared_ptr<lm::ngram::State> in_state = model->get_begin_state();

    std::vector<std::string> sentence = {"language", "modeling", "is", "fun", "</s>"};
    std::vector<float> scores;

    // Get target scores with python script
    std::vector<float> targets = {-2.410608292, -15.0, -23.687871933, -2.296664953, -21.029493332};
    float target = -64.424636841;

    for (std::string word : sentence)
    {
        std::pair<float, std::shared_ptr<lm::ngram::State>> result = model->score_word(word, in_state);
        in_state = std::move(result.second);
        scores.push_back(result.first);
    }

    float score = 0.0;
    for (size_t i = 0; i < targets.size(); i++)
    {
        score += scores[i];
        if (std::abs(targets[i] - scores[i]) > 0.001)
        {
            success = false;
            std::cerr << "\nERROR: Word scoring incorrect!" << std::endl;
            std::cerr << "Got: " << targets[i] << " vs " << scores[i] << std::endl;
        }
    }

    if (std::abs(target - score) > 0.001)
    {
        success = false;
        std::cerr << "\nERROR: Sentence scoring incorrect!" << std::endl;
        std::cerr << "Got: " << target << " vs " << score << std::endl;
        std::cerr << std::endl;
    }

    if (!success)
    {
        throw(std::runtime_error("Tests failed!"));
    }
}

// =================================================================================================

void test_repeated()
{
    std::string lm_path = "/kenlm/lm/test.arpa";
    bool success = true;

    std::unique_ptr<Langmodel> model = std::make_unique<Langmodel>(lm_path, 1.0, 0.0, false);
    std::shared_ptr<lm::ngram::State> in_state;

    std::vector<std::string> sentence1 = {"language", "modeling", "is", "fun", "</s>"};
    in_state = std::move(model->get_begin_state());
    float score1 = 0.0;
    for (std::string word : sentence1)
    {
        std::pair<float, std::shared_ptr<lm::ngram::State>> result = model->score_word(word, in_state);
        in_state = std::move(result.second);
        score1 += result.first;
    }

    std::vector<std::string> sentence2 = {"looking", "on", "a", "little", "more", "</s>"};
    in_state = std::move(model->get_begin_state());
    float score2 = 0.0;
    for (std::string word : sentence2)
    {
        std::pair<float, std::shared_ptr<lm::ngram::State>> result = model->score_word(word, in_state);
        in_state = std::move(result.second);
        score2 += result.first;
    }

    score1 = std::pow(10, score1);
    score2 = std::pow(10, score2);
    if (score1 > score2)
    {
        success = false;
        std::cerr << "\nERROR: Scoring incorrect!\n";
        std::cerr << "Got: " << score1 << " vs " << score2 << std::endl;
        std::cerr << std::endl;
    }

    if (!success)
    {
        throw(std::runtime_error("Tests failed!"));
    }
}

// =================================================================================================

void test_append()
{
    std::string lm_path = "/kenlm/lm/test.arpa";
    bool success = true;

    // In a presumably wrong installation, appending words to the sentence resulted in memory errors
    std::vector<std::string> sentence = {"language", "modeling", "is", "fun"};
    sentence.push_back("</s>");

    std::unique_ptr<Langmodel> model = std::make_unique<Langmodel>(lm_path, 1.0, 0.0, true);
    std::shared_ptr<lm::ngram::State> in_state = model->get_begin_state();

    float target = -64.424636841;
    float score = 0;

    for (std::string word : sentence)
    {
        std::pair<float, std::shared_ptr<lm::ngram::State>> result = model->score_word(word, in_state);
        in_state = std::move(result.second);
        score += result.first;
        std::cout << word << " " << result.first << std::endl;
    }
    std::cout << "total: " << score << std::endl;

    if (std::abs(target - score) > 0.001)
    {
        success = false;
        std::cerr << "\nERROR: Sentence scoring incorrect!" << std::endl;
        std::cerr << "Got: " << target << " vs " << score << std::endl;
    }

    if (!success)
    {
        throw(std::runtime_error("Tests failed!"));
    }
}

// =================================================================================================

int main()
{
    test_scoring();
    test_repeated();
    test_append();

    std::cout << "--finished--" << std::endl;
    return 0;
}
