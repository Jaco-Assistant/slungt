#include <iostream>
#include <string>
#include "lm/model.hh"

int main()
{
    using namespace lm::ngram;

    Model model("/kenlm/lm/test.arpa");
    std::vector<std::string> words = {"language", "modeling", "is", "fun", "</s>"};

    State state(model.BeginSentenceState()), out_state;
    const Vocabulary &vocab = model.GetVocabulary();

    float total = 0.0;
    for (std::string word : words)
    {
        float score = model.Score(state, vocab.Index(word), out_state);
        std::cout << word << " " << score << std::endl;
        state = out_state;
        total += score;
    }

    std::cout << "total: " << total << std::endl;
    std::cout << "--finished--" << std::endl;
    return 0;
}
