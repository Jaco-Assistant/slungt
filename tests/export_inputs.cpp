#include <filesystem>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#include "input_values.hpp"

// =================================================================================================

void save_as_json(
    std::string outpath,
    std::string text,
    std::string intent,
    std::string intenttext,
    std::vector<std::string> alphabet,
    std::vector<std::vector<float>> ctc_tokens)
{
    std::ofstream outFile(outpath);

    // Begin JSON-like structure
    outFile << "{" << std::endl;

    // Write each variable in JSON-like format
    outFile << "\"text\": \"" << text << "\"," << std::endl;
    outFile << "\"intent\": \"" << intent << "\"," << std::endl;
    outFile << "\"intenttext\": \"" << intenttext << "\"," << std::endl;

    outFile << "\"alphabet\": [";
    for (size_t i = 0; i < alphabet.size(); ++i)
    {
        outFile << "\"" << alphabet[i] << "\"";
        if (i != alphabet.size() - 1)
        {
            outFile << ", ";
        }
    }
    outFile << "]," << std::endl;

    outFile << "\"ctc_tokens\": [";
    for (size_t i = 0; i < ctc_tokens.size(); ++i)
    {
        outFile << "[";
        for (size_t j = 0; j < ctc_tokens[i].size(); ++j)
        {
            outFile << ctc_tokens[i][j];
            if (j != ctc_tokens[i].size() - 1)
            {
                outFile << ", ";
            }
        }
        outFile << "]";
        if (i != ctc_tokens.size() - 1)
        {
            outFile << ", ";
        }
    }
    outFile << "]" << std::endl;

    // End JSON-like structure
    outFile << "}" << std::endl;

    outFile.close();
}

// =================================================================================================

int main()
{
    std::string filepath = std::filesystem::current_path().string();
    std::filesystem::path out_dir = std::filesystem::path(filepath) / "outputs/test_input_values/";

    if (!std::filesystem::exists(out_dir))
    {
        std::filesystem::create_directory(out_dir);
    }

    save_as_json(out_dir / "input1.json", text1, "", "", alphabet1, ctc_tokens1);
    save_as_json(out_dir / "input5.json", text5, intent5, intenttext5, alphabet5, ctc_tokens5);
    save_as_json(out_dir / "input6.json", text6, intent6, intenttext6, alphabet6, ctc_tokens6);
    save_as_json(out_dir / "input8.json", text8, intent8, intenttext8, alphabet8, ctc_tokens8);

    std::cout << "Exported inputs" << std::endl;
    return 0;
}
