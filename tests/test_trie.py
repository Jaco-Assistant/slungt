import sys
import time

sys.path.append("/slungt/swig/")
import slungt  # pylint: disable=wrong-import-position

# Disable warning for slungt attributes
# mypy: disable-error-code="attr-defined"

# ==================================================================================================


def test_basic_ops() -> None:
    # Create an emtpy trie
    trie = slungt.Trie()

    # Insert all words
    words = ["answer", "dog", "gray cat", "devours", "[dog](answer)", "dog eater"]
    trie.insert_words(words)

    # Test counting
    result = trie.count_words()
    if result != 5:
        print("\nERROR: Wrong number of complete words!")
        print("Found:", result)
        print("")

    # Test contains
    queriesC = ["answer ", "question ", "dog ", "cat ", "answ", "doggo ", "dog eater "]
    labelsC = [
        slungt.TrieState_CompleteNoChild,
        slungt.TrieState_Missing,
        slungt.TrieState_CompleteHasChild,
        slungt.TrieState_Missing,
        slungt.TrieState_Contained,
        slungt.TrieState_Missing,
        slungt.TrieState_CompleteNoChild,
    ]
    for i in range(len(queriesC)):
        if trie.check_state(queriesC[i]) != labelsC[i]:
            print("\nERROR: Word status is wrong!")
            print("'" + str(queriesC[i]) + "'")
            print("")

    # Test search
    queriesS = ["dog ", "cat ", "devours "]
    labelsS = [["[dog](answer)", "dog"], [""], ["devours"]]
    for i in range(len(queriesS)):
        result = trie.search(queriesS[i])
        if set(result) != set(labelsS[i]):
            print("\nERROR: Search gave incorrect result!")
            print("'" + queriesS[i] + "':", result)
            print("")

    print("\nCompleted tests")


# ==================================================================================================


def test_many_words(path: str) -> None:
    with open(path, "r", encoding="utf-8") as file:
        words = file.readlines()
        words = [w.strip() for w in words]

    # Create a trie
    stime = time.time()
    trie = slungt.Trie()
    trie.insert_words(words)
    stime = time.time() - stime
    print("Filling trie with {} words took {:.3f}s".format(len(words), stime))

    stime = time.time()
    for w in words[::10]:
        if trie.check_state(w) != slungt.TrieState_Contained:
            print("Missing:", w)
    stime = time.time() - stime
    print(
        "Querying {} words took {:.3f}s or {:.3f}ns per word".format(
            len(words[::10]), stime, (stime / len(words[::10])) * 1e9
        )
    )


# ==================================================================================================

if __name__ == "__main__":
    test_basic_ops()
    test_many_words("/slungt/tests/data/words.txt")
    test_many_words("/slungt/tests/data/woerter-derewo-bearbeitet.txt")
