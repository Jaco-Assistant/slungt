#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "../slungt/interface.hpp"
#include "../slungt/utils.hpp"
#include "input_values.hpp"

// =================================================================================================

void log_error(
    std::string tid,
    std::vector<BeamResult> results,
    std::string target_text = "",
    std::string target_intent = "",
    std::string greedy_text = "")
{
    std::cout << "\nERROR: Prediction incorrect! (" << tid << ")\n";

    if (!target_text.empty())
    {
        std::cout << "Text:\n";
        std::cout << "  Got: '" << results[0].text << "'\n";
        std::cout << "  vs:  '" << target_text << "'\n";
    }

    if (!target_intent.empty())
    {
        std::cout << "Intent:\n";
        std::cout << "  Got: '" << results[0].intent << "'\n";
        std::cout << "  vs:  '" << target_intent << "'\n";
    }

    if (!greedy_text.empty())
    {
        std::cout << "Greedy: " << greedy_text << "\n";
    }

    if (results.size() > 1)
    {
        std::cout << "Beams:\n";
        for (const auto &r : results)
        {
            std::cout << "  " << r << "\n";
        }
    }

    std::cout << std::string(100, '-') << "\n";
}

// =================================================================================================

std::vector<std::string> read_lines(const std::string &path)
{
    std::ifstream file(path);
    std::vector<std::string> lines;

    if (file.is_open())
    {
        std::string line;
        while (std::getline(file, line))
        {
            if (!line.empty())
            {
                line = strip_text(line);
                lines.push_back(line);
            }
        }
        file.close();
    }

    return lines;
}

// =================================================================================================

std::tuple<
    std::vector<std::string>, std::vector<std::vector<std::string>>, std::vector<std::string>>
load_data(const std::filesystem::path dataPath, const bool use_binary_lm)
{

    std::vector<std::string> intents_riddle;
    std::vector<std::vector<std::string>> vocabs_riddle;
    std::vector<std::string> lms_riddle;

    std::filesystem::path sentsPath = dataPath / "sents";
    for (const auto &entry : std::filesystem::directory_iterator(sentsPath))
    {
        if (entry.is_regular_file())
        {
            std::string filename = entry.path().filename().string();
            std::string intent = filename.substr(0, filename.find(".txt"));
            intents_riddle.push_back(intent);

            std::filesystem::path vocabPath = dataPath / "vocab" / filename;
            std::vector<std::string> lines = read_lines(vocabPath.string());
            vocabs_riddle.push_back(lines);

            std::filesystem::path lmPath = dataPath / "arpas" / filename;
            if (!use_binary_lm)
                lmPath.replace_extension(".arpa");
            else
                lmPath.replace_extension(".bin");

            lms_riddle.push_back(lmPath.string());
        }
    }

    return std::make_tuple(intents_riddle, vocabs_riddle, lms_riddle);
}

// =================================================================================================

void test_simple_predictions()
{
    bool success = true;

    Decoder decoder1 = Decoder(
        alphabet1, 3, {}, {}, {}, 1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::string greedy1 = decoder1.decode_greedy_text(ctc_tokens1);
    std::cout << greedy1 << std::endl;
    if (greedy1 != text1)
    {
        log_error("1a", {}, text1, "", greedy1);
        success = false;
    }

    std::vector<BeamResult> results1 = decoder1.decode(ctc_tokens1);
    std::cout << results1[0] << std::endl;
    if (results1[0].text != text1)
    {
        log_error("1b", results1, text1, "", "");
        success = false;
    }

    Decoder decoder2 = Decoder(
        alphabet2, 3, {}, {}, {}, 1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results2 = decoder2.decode(ctc_tokens2);
    std::cout << results2[0] << std::endl;
    if (results2[0].text != text2)
    {
        log_error("2a", results2, text2, "", "");
        success = false;
    }

    std::vector<std::vector<float>> ctc_tokens21 = ctc_tokens2;
    ctc_tokens21[0] = {0.2, 0.2, 0.4, 0.2};
    std::vector<BeamResult> results21 = decoder2.decode(ctc_tokens21);
    std::cout << results21[0] << std::endl;
    if (results21[0].text != text2)
    {
        log_error("2b", results21, text2, "", "");
        success = false;
    }
    if (results21[0].score >= results2[0].score)
    {
        success = false;
        std::cout << "\nERROR: Logit score incorrect!\n";
        std::cout << "Got: " << results21[0].score << " vs " << results2[0].score << "\n";
    }

    Decoder decoder3 = Decoder(
        alphabet3, 3, {}, {}, {}, 1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results3 = decoder3.decode(ctc_tokens3);
    std::cout << results3[0] << std::endl;
    if (results3[0].text != text3)
    {
        log_error("3", results3, text3, "", "");
        success = false;
    }

    Decoder decoder4 = Decoder(
        alphabet4, 3, {}, {}, {}, 1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results4 = decoder4.decode(ctc_tokens4);
    std::cout << results4[0] << std::endl;
    if (results4[0].text != text4)
    {
        log_error("4", results4, text4, "", "");
        success = false;
    }

    Decoder decoder5 = Decoder(
        alphabet5, 3, {}, {}, {}, 1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results5 = decoder5.decode(ctc_tokens5);
    std::cout << results5[0] << std::endl;
    if (results5[0].text != text5)
    {
        log_error("5", results5, text5, "", "");
        success = false;
    }

    Decoder decoder6 = Decoder(
        alphabet6, 3, {}, {}, {}, 1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results6 = decoder6.decode(ctc_tokens6);
    std::cout << results6[0] << std::endl;
    if (results6[0].text != text6)
    {
        log_error("6", results6, text6, "", "");
        success = false;
    }

    Decoder decoder7 = Decoder(
        alphabet7, 30, {}, {}, {}, 1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results7 = decoder7.decode(ctc_tokens7);
    std::cout << results7[0] << std::endl;
    if (results7[0].text != text7)
    {
        log_error("7", results7, text7, "", "");
        success = false;
    }

    Decoder decoder8 = Decoder(
        alphabet8, 30, {}, {}, {}, 1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results8 = decoder8.decode(ctc_tokens8);
    std::cout << results8[0] << std::endl;
    if (results8[0].text != text8)
    {
        log_error("8", results8, text8, "", "");
        success = false;
    }

    if (!success)
    {
        throw std::runtime_error("Tests failed");
    }
}

// =================================================================================================

void test_trie_predictions()
{
    bool success = true;
    std::string filepath = std::filesystem::current_path().string();

    std::filesystem::path path_riddle = std::filesystem::path(filepath) / "outputs/riddles/";
    auto [intents_riddle, vocabs_riddle, lms_riddle] = load_data(path_riddle, true);

    Decoder decoder4 = Decoder(
        alphabet4, 10, vocabs_riddle, intents_riddle, {},
        1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results4 = decoder4.decode(ctc_tokens4);
    std::cout << results4[0] << std::endl;
    if (results4[0].text != intenttext4 || results4[0].intent != intent4)
    {
        log_error("4", results4, intenttext4, intent4, "");
        success = false;
    }

    Decoder decoder9 = Decoder(
        alphabet9, 2, vocabs_riddle, intents_riddle, {},
        1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results9 = decoder9.decode(ctc_tokens9);
    std::cout << results9[0] << std::endl;
    if (results9[0].text != intenttext9 || results9[0].intent != intent9)
    {
        log_error("9", results9, intenttext9, intent9, "");
        success = false;
    }

    Decoder decoder10 = Decoder(
        alphabet10, 5, vocabs_riddle, intents_riddle, {},
        1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results10 = decoder10.decode(ctc_tokens10);
    std::cout << results10[0] << std::endl;
    if (results10[0].text != intenttext10 || results10[0].intent != intent10)
    {
        log_error("10", results10, intenttext10, intent10, "");
        success = false;
    }

    std::filesystem::path path_sle = std::filesystem::path(filepath) / "outputs/smartlights/";
    auto [intents_sle, vocabs_sle, lms_sle] = load_data(path_sle, true);

    Decoder decoder5 = Decoder(
        alphabet5, 30, vocabs_sle, intents_sle, {},
        1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results5 = decoder5.decode(ctc_tokens5);
    std::cout << results5[0] << std::endl;
    if (results5[0].text != intenttext5 || results5[0].intent != intent5)
    {
        log_error("5", results5, intenttext5, intent5, "");
        success = false;
    }

    Decoder decoder6 = Decoder(
        alphabet6, 300, vocabs_sle, intents_sle, {},
        1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results6 = decoder6.decode(ctc_tokens6);
    std::cout << results6[0] << std::endl;
    if (results6[0].text != intenttext6 || results6[0].intent != intent6)
    {
        log_error("6", results6, intenttext6, intent6, "");
        success = false;
    }

    Decoder decoder7 = Decoder(
        alphabet7, 30, vocabs_sle, intents_sle, {},
        1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results7 = decoder7.decode(ctc_tokens7);
    std::cout << results7[0] << std::endl;
    if (results7[0].text != intenttext7 || results7[0].intent != intent7)
    {
        log_error("7", results7, intenttext7, intent7, "");
        success = false;
    }

    Decoder decoder8 = Decoder(
        alphabet8, 30, vocabs_sle, intents_sle, {},
        1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results8 = decoder8.decode(ctc_tokens8);
    std::cout << results8[0] << std::endl;
    if (results8[0].text != intenttext8 || results8[0].intent != intent8)
    {
        log_error("8", results8, intenttext8, intent8, "");
        success = false;
    }

    std::filesystem::path path_barrista = std::filesystem::path(filepath) / "outputs/barrista/";
    auto [intents_barrista, vocabs_barrista, lms_barrista] = load_data(path_barrista, true);

    Decoder decoder12 = Decoder(
        alphabet12, 30, vocabs_barrista, intents_barrista, {},
        1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results12 = decoder12.decode(ctc_tokens12);
    std::cout << results12[0] << std::endl;
    if (results12[0].text != intenttext12 || results12[0].intent != intent12)
    {
        log_error("12", results12, intenttext12, intent12, "");
        success = false;
    }

    Decoder decoder13 = Decoder(
        alphabet13, 50, vocabs_barrista, intents_barrista, {},
        1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results13 = decoder13.decode(ctc_tokens13);
    std::cout << results13[0] << std::endl;
    if (results13[0].text != intenttext13 || results13[0].intent != intent13)
    {
        log_error("13", results13, intenttext13, intent13, "");
        success = false;
    }

    Decoder decoder18 = Decoder(
        alphabet18, 2, vocabs_barrista, intents_barrista, {},
        1.5, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results18 = decoder18.decode(ctc_tokens18);
    std::cout << results18[0] << std::endl;
    if (results18[0].text != intenttext18 || results18[0].intent != intent18)
    {
        log_error("18", results18, intenttext18, intent18, "");
        success = false;
    }

    if (!success)
    {
        throw std::runtime_error("Tests failed");
    }
}

// =================================================================================================

void test_lm_predictions()
{
    bool success = true;
    std::string filepath = std::filesystem::current_path().string();

    std::filesystem::path path_riddle = std::filesystem::path(filepath) / "outputs/riddles/";
    auto [intents_riddle, vocabs_riddle, lms_riddle] = load_data(path_riddle, false);

    Decoder decoder4 = Decoder(
        alphabet4, 10, vocabs_riddle, intents_riddle, lms_riddle,
        1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results4 = decoder4.decode(ctc_tokens4);
    std::cout << results4[0] << std::endl;
    if (results4[0].text != intenttext4 || results4[0].intent != intent4)
    {
        log_error("4", results4, intenttext4, intent4, "");
        success = false;
    }

    std::filesystem::path path_sle = std::filesystem::path(filepath) / "outputs/smartlights/";
    auto [intents_sle, vocabs_sle, lms_sle] = load_data(path_sle, true);

    Decoder decoder5 = Decoder(
        alphabet5, 30, vocabs_sle, intents_sle, lms_sle,
        1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results5 = decoder5.decode(ctc_tokens5);
    std::cout << results5[0] << std::endl;
    if (results5[0].text != intenttext5 || results5[0].intent != intent5)
    {
        log_error("5", results5, intenttext5, intent5, "");
        success = false;
    }

    Decoder decoder6 = Decoder(
        alphabet6, 300, vocabs_sle, intents_sle, lms_sle,
        1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results6 = decoder6.decode(ctc_tokens6);
    std::cout << results6[0] << std::endl;
    if (results6[0].text != intenttext6 || results6[0].intent != intent6)
    {
        log_error("6", results6, intenttext6, intent6, "");
        success = false;
    }

    Decoder decoder7 = Decoder(
        alphabet7, 30, vocabs_sle, intents_sle, lms_sle,
        1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results7 = decoder7.decode(ctc_tokens7);
    std::cout << results7[0] << std::endl;
    if (results7[0].text != intenttext7 || results7[0].intent != intent7)
    {
        log_error("7", results7, intenttext7, intent7, "");
        success = false;
    }

    Decoder decoder8 = Decoder(
        alphabet8, 30, vocabs_sle, intents_sle, lms_sle,
        1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results8 = decoder8.decode(ctc_tokens8);
    std::cout << results8[0] << std::endl;
    if (results8[0].text != intenttext8 || results8[0].intent != intent8)
    {
        log_error("8", results8, intenttext8, intent8, "");
        success = false;
    }

    std::filesystem::path path_barrista = std::filesystem::path(filepath) / "outputs/barrista/";
    auto [intents_barrista, vocabs_barrista, lms_barrista] = load_data(path_barrista, true);

    Decoder decoder11 = Decoder(
        alphabet11, 50, vocabs_barrista, intents_barrista, lms_barrista,
        1.0, 1.0, 1.0, -15.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results11 = decoder11.decode(ctc_tokens11);
    std::cout << results11[0] << std::endl;
    if (results11[0].text != intenttext11 || results11[0].intent != intent11)
    {
        log_error("11", results11, intenttext11, intent11, "");
        success = false;
    }

    Decoder decoder12 = Decoder(
        alphabet12, 30, vocabs_barrista, intents_barrista, lms_barrista,
        1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results12 = decoder12.decode(ctc_tokens12);
    std::cout << results12[0] << std::endl;
    if (results12[0].text != intenttext12 || results12[0].intent != intent12)
    {
        log_error("12", results12, intenttext12, intent12, "");
        success = false;
    }

    Decoder decoder13 = Decoder(
        alphabet13, 50, vocabs_barrista, intents_barrista, lms_barrista,
        1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results13 = decoder13.decode(ctc_tokens13);
    std::cout << results13[0] << std::endl;
    if (results13[0].text != intenttext13 || results13[0].intent != intent13)
    {
        log_error("13", results13, intenttext13, intent13, "");
        success = false;
    }

    Decoder decoder14 = Decoder(
        alphabet14, 200, vocabs_barrista, intents_barrista, lms_barrista,
        1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results14 = decoder14.decode(ctc_tokens14);
    std::cout << results14[0] << std::endl;
    if (results14[0].text != intenttext14 || results14[0].intent != intent14)
    {
        log_error("14", results14, intenttext14, intent14, "");
        success = false;
    }

    Decoder decoder15 = Decoder(
        alphabet15, 20, vocabs_barrista, intents_barrista, lms_barrista,
        1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results15 = decoder15.decode(ctc_tokens15);
    std::cout << results15[0] << std::endl;
    if (results15[0].text != intenttext15 || results15[0].intent != intent15)
    {
        log_error("15", results15, intenttext15, intent15, "");
        success = false;
    }

    Decoder decoder16 = Decoder(
        alphabet16, 50, vocabs_barrista, intents_barrista, lms_barrista,
        1.5, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results16 = decoder16.decode(ctc_tokens16);
    std::cout << results16[0] << std::endl;
    if (results16[0].text != intenttext16 || results16[0].intent != intent16)
    {
        log_error("16", results16, intenttext16, intent16, "");
        success = false;
    }

    std::filesystem::path path_tas = std::filesystem::path(filepath) / "outputs/timersandsuch/";
    auto [intents_tas, vocabs_tas, lms_tas] = load_data(path_tas, true);

    Decoder decoder17 = Decoder(
        alphabet17, 50, vocabs_tas, intents_tas, lms_tas,
        1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results17 = decoder17.decode(ctc_tokens17);
    std::cout << results17[0] << std::endl;
    if (results17[0].text != intenttext17 || results17[0].intent != intent17)
    {
        log_error("17", results17, intenttext17, intent17, "");
        success = false;
    }

    Decoder decoder19 = Decoder(
        alphabet19, 50, vocabs_tas, intents_tas, lms_tas,
        1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results19 = decoder19.decode(ctc_tokens19);
    std::cout << results19[0] << std::endl;
    if (results19[0].text != intenttext19 || results19[0].intent != intent19)
    {
        log_error("19", results19, intenttext19, intent19, "");
        success = false;
    }

    Decoder decoder20 = Decoder(
        alphabet20, 50, vocabs_tas, intents_tas, lms_tas,
        1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);
    std::vector<BeamResult> results20 = decoder20.decode(ctc_tokens20);
    std::cout << results20[0] << std::endl;
    if (results20[0].text != intenttext20 || results20[0].intent != intent20)
    {
        log_error("20", results20, intenttext20, intent20, "");
        success = false;
    }

    if (!success)
    {
        throw std::runtime_error("Tests failed");
    }
}

// =================================================================================================

void test_intent_conversion()
{
    std::string itext = "the solution of the riddle is [dog->a pet](skill_riddles-riddle_answers)";
    BeamResult beam_result = {itext, "skill_riddles-check_riddle", 1.0};

    Decoder decoder = Decoder(
        {}, 1, {}, {}, {}, 1.0, 1.0, 1.0, -10.0, 64, -20.0, -10.0, 6, 0.01, 0.001, {}, -1);

    Intent converted = decoder.extract_text2intent(beam_result);
    std::cout << converted << std::endl;

    Intent target = {
        "skill_riddles-check_riddle",
        "the solution of the riddle is dog",
        {{"skill_riddles-riddle_answers", "", "a pet"}}};

    if (converted.text != target.text ||
        converted.name != target.name ||
        converted.entities[0].name != target.entities[0].name ||
        converted.entities[0].role != target.entities[0].role ||
        converted.entities[0].value != target.entities[0].value)
    {
        std::cout << "input: " << beam_result << "\ntarget: "
                  << target << "\ngot:    " << converted << std::endl;
        throw std::runtime_error("Tests failed");
    }
}

// =================================================================================================

int main()
{
    test_simple_predictions();
    std::cout << std::endl;

    test_trie_predictions();
    std::cout << std::endl;

    test_lm_predictions();
    std::cout << std::endl;

    test_intent_conversion();
    std::cout << std::endl;

    std::cout << "Completed Decoder tests" << std::endl;
    return 0;
}
