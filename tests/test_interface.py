import json
import os
import sys
from typing import List, Tuple

filepath = os.path.dirname(os.path.abspath(__file__)) + "/"
sys.path.append(filepath + "../swig/")
import slungt  # pylint: disable=wrong-import-position

# Disable warning for slungt attributes
# mypy: disable-error-code="attr-defined"

# ==================================================================================================


def read_lines(path: str) -> List[str]:
    with open(path, "r", encoding="utf-8") as file:
        lines = file.readlines()
    lines = [ln for ln in lines if not ln == ""]
    return lines


# ==================================================================================================


def load_data(path: str) -> Tuple[List[List[str]], List[str], List[str]]:
    files = os.listdir(path + "sents/")
    intents = [fn.replace(".txt", "") for fn in files]
    vocabs = [read_lines(os.path.join(path, "vocab/", fn)) for fn in files]
    vocabs = [[w.strip() for w in v] for v in vocabs]
    lms = [os.path.join(path, "arpas/", fn) for fn in files]
    lms = [fn.replace(".txt", ".bin") for fn in lms]

    return vocabs, intents, lms


# ==================================================================================================


def main() -> None:
    print("")
    input_dir = filepath + "outputs/test_input_values/"

    # Test beam-result structure
    testresult = slungt.BeamResult()
    testresult.text = "test"
    testresult.intent = "intent"
    testresult.score = 0.99
    testresult.tokens = ("t", "e", "s", "t")
    print(testresult)
    print("")

    # Test intent structure
    testintent = slungt.Intent()
    testintent.name = "intent"
    testintent.text = "test"
    testentity1 = slungt.Entity()
    testentity1.name = "entity1"
    testentity1.value = "value1"
    testentity1.role = "value1"
    testentity2 = slungt.Entity()
    testentity2.name = "entity2"
    testentity2.value = "value2"
    testentity2.role = "value2"
    testintent.entities = (testentity1, testentity2)
    print(testintent)
    print("")

    # Test basic decoding
    with open(input_dir + "input1.json", "r", encoding="utf-8") as file:
        input1 = json.load(file)
    decoder1 = slungt.Decoder(input1["alphabet"], max_beam_size=3)
    greedy1 = decoder1.decode_greedy_text(input1["ctc_tokens"])
    assert greedy1 == input1["text"], greedy1
    results1 = decoder1.decode(input1["ctc_tokens"])
    assert results1[0].text == input1["text"], results1[0]

    sle_dir = filepath + "outputs/smartlights/"
    vocabs_sle, intents_sle, lms_sle = load_data(sle_dir)

    # Test trie decoding
    with open(input_dir + "input5.json", "r", encoding="utf-8") as file:
        input5 = json.load(file)
    decoder5 = slungt.Decoder(input5["alphabet"], 30, vocabs_sle, intents_sle)
    results5 = decoder5.decode(input5["ctc_tokens"])
    print(results5[0])
    assert (
        results5[0].text == input5["intenttext"]
        and results5[0].intent == input5["intent"]
    )

    # Test language model decoding with charbased inputs
    with open(input_dir + "input6.json", "r", encoding="utf-8") as file:
        input6 = json.load(file)
    decoder6 = slungt.Decoder(input6["alphabet"], 300, vocabs_sle, intents_sle, lms_sle)
    results6 = decoder6.decode(input6["ctc_tokens"])
    print(results6[0])
    assert (
        results6[0].text == input6["intenttext"]
        and results6[0].intent == input6["intent"]
    )

    # Test language model decoding with sentencepiece inputs
    with open(input_dir + "input8.json", "r", encoding="utf-8") as file:
        input8 = json.load(file)
    decoder8 = slungt.Decoder(
        input8["alphabet"],
        30,
        vocabs_sle,
        intents_sle,
        lms_sle,
        token_min_logprob=-10.0,
    )
    results8 = decoder8.decode(input8["ctc_tokens"])
    print(results8[0])
    assert (
        results8[0].text == input8["intenttext"]
        and results8[0].intent == input8["intent"]
    )

    # Test streamed decoding
    decoder8.reset()
    for token in input8["ctc_tokens"]:
        decoder8.decode_step(token)
    results8 = decoder8.finalize()
    print(results8[0])
    assert (
        results8[0].text == input8["intenttext"]
        and results8[0].intent == input8["intent"]
    )

    print("Completed tests")


# ==================================================================================================

if __name__ == "__main__":
    main()
