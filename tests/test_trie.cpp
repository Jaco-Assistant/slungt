#include <chrono>
#include <fstream>
#include <iostream>
#include <set>
#include <string>
#include <vector>

#include "../slungt/trie_state.hpp"
#include "../slungt/trie.hpp"

// =================================================================================================

void test_tags()
{
    bool success = true;

    // Create an emtpy trie
    Trie *trie = new Trie();

    // Test tag splitting without and with synonyms
    std::string teststr1 = "[dog](answer)";
    std::array<std::string, 3> ext1 = trie->extract_tags(teststr1);
    std::array<std::string, 3> label1 = {"dog", "[", "](answer)"};
    if (ext1 != label1)
    {
        success = false;
        std::cerr << "\nERROR: Tags not correctly extracted!" << std::endl;
        std::cerr << "Got: '" << ext1[0] << "' '" << ext1[1] << "' '" << ext1[2] << "'" << std::endl;
        std::cerr << std::endl;
    }
    std::string teststr2 = "[black cat->gray cat](answer)";
    std::array<std::string, 3> ext2 = trie->extract_tags(teststr2);
    std::array<std::string, 3> label2 = {"black cat", "[", "->gray cat](answer)"};
    if (ext2 != label2)
    {
        success = false;
        std::cerr << "\nERROR: Tags not correctly extracted!" << std::endl;
        std::cerr << "Got: '" << ext2[0] << "' '" << ext2[1] << "' '" << ext2[2] << "'" << std::endl;
        std::cerr << std::endl;
    }

    if (!success)
    {
        throw(std::runtime_error("Tests failed!"));
    }
}

// =================================================================================================

void test_basic()
{
    bool success = true;

    // Create an emtpy trie
    Trie *trie = new Trie();

    // Insert all words
    std::vector<std::string> words = {"answer", "dog", "gray cat", "devours", "[dog](answer)"};
    trie->insert_words(words);

    // Test counting
    int result = trie->count_words();
    if (result != 4)
    {
        success = false;
        std::cerr << "\nERROR: Wrong number of complete words!" << std::endl;
        std::cerr << "Found: " << result << "" << std::endl;
        std::cerr << std::endl;
    }

    if (!success)
    {
        throw(std::runtime_error("Tests failed!"));
    }
}

// =================================================================================================

void test_search()
{
    bool success = true;

    // Create a new trie
    Trie *trie = new Trie();
    std::vector<std::string> words;
    words = {"answer", "dog", "gray cat", "devours", "[dog](answer)", "dog eater"};
    trie->insert_words(words);

    // Test states
    std::vector<std::string> queriesC;
    queriesC = {"answer ", "question ", "dog ", "cat ", "answ", "doggo ", "dog eater "};
    std::vector<TrieState> labelsC = {
        TrieState::CompleteNoChild,
        TrieState::Missing,
        TrieState::CompleteHasChild,
        TrieState::Missing,
        TrieState::Contained,
        TrieState::Missing,
        TrieState::CompleteNoChild,
    };
    for (size_t i = 0; i < queriesC.size(); i++)
    {
        if (trie->check_state(queriesC[i]) != labelsC[i])
        {
            success = false;
            std::cerr << "\nERROR: Word status is wrong!" << std::endl;
            std::cerr << "'" << queriesC[i] << "'" << std::endl;
            std::cerr << std::endl;
        }
    }

    // Test search
    std::vector<std::string> queriesS = {"dog ", "cat ", "devours "};
    std::vector<std::vector<std::string>> labelsS = {{"[dog](answer)", "dog"}, {""}, {"devours"}};
    for (size_t i = 0; i < queriesS.size(); i++)
    {
        std::vector<std::string> result = trie->search(queriesS[i]);

        std::set<std::string> set_label(labelsS[i].begin(), labelsS[i].end());
        std::set<std::string> set_result(result.begin(), result.end());

        if (set_result != set_label)
        {
            success = false;
            std::cerr << "\nERROR: Search gave incorrect result!" << std::endl;
            std::cerr << "'" << queriesS[i] << "':";
            for (std::string s : result)
            {
                std::cerr << " '" << s << "'";
            }
            std::cerr << "\n"
                      << std::endl;
        }
    }

    if (!success)
    {
        throw(std::runtime_error("Tests failed!"));
    }
}

// =================================================================================================

void test_special_words()
{
    bool success = true;

    // Create a new trie
    Trie *trie = new Trie();
    std::vector<std::string> words = {
        "[abc->xyz](simple abc)",
        "skill_riddles-riddle_answers",
        "[abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz](double abc)",
        "one",
        "one two",
        "one two three",
        "one two three four",
        "one two three four five",
    };
    trie->insert_words(words);

    std::string query1 = "skill_riddles-riddle_answ";
    if (trie->check_state(query1) != TrieState::Contained)
    {
        success = false;
        std::cerr << "\nERROR: Long partial word not found!\n";
        std::cerr << "\n";
    }

    std::string query2 = "abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz ";
    std::string result2 = trie->search(query2)[0];
    if (result2 != words[2])
    {
        success = false;
        std::cerr << result2 << std::endl;
        std::cerr << "\nERROR: Long word is missing!" << std::endl;
        std::cerr << std::endl;
    }

    std::string query3 = "abc ";
    std::string result3 = trie->search(query3)[0];
    if (result3 != words[0])
    {
        success = false;
        std::cerr << result3 << std::endl;
        std::cerr << "\nERROR: Synonym is missing!" << std::endl;
        std::cerr << std::endl;
    }

    std::string query4a = "one ";
    std::string query4b = "one two three ";
    std::string query4c = "one two three four five ";
    if (trie->check_state(query4a) != TrieState::CompleteHasChild || trie->check_state(query4b) != TrieState::CompleteHasChild || trie->check_state(query4c) != TrieState::CompleteNoChild)
    {
        std::cerr << "\nERROR: Long multi-space word not found!\n";
        std::cerr << "\n";
    }

    if (!success)
    {
        throw(std::runtime_error("Tests failed!"));
    }
}

// =================================================================================================

void test_incomplete_words()
{
    bool success = true;

    // Create a new trie
    Trie *trie = new Trie();
    std::vector<std::string> words = {
        "dog",
        "dog eater",
        "doggo",
    };
    trie->insert_words(words);

    // Test states
    std::vector<std::string> queriesC;
    queriesC = {"dog eat", "dodo", "dog burger", "dogg", "dog eater bad"};
    std::vector<TrieState> labelsC = {
        TrieState::Contained,
        TrieState::Missing,
        TrieState::Missing,
        TrieState::Contained,
        TrieState::Missing,
    };
    for (size_t i = 0; i < queriesC.size(); i++)
    {
        if (trie->check_state(queriesC[i]) != labelsC[i])
        {
            success = false;
            std::cerr << "\nERROR: Word status is wrong!" << std::endl;
            std::cerr << "'" << queriesC[i] << "'" << std::endl;
            std::cerr << std::endl;
        }
    }

    if (!success)
    {
        throw(std::runtime_error("Tests failed!"));
    }
}

// =================================================================================================

void test_special_chars()
{
    bool success = true;

    // Create a new trie
    Trie *trie = new Trie();
    std::vector<std::string> words = {
        "abc",
        "äöüß",
        "ñáéíóúüý",
        "' àâæçéèêëîïôœùûüÿ",
    };
    trie->insert_words(words);

    // Test states
    for (size_t i = 0; i < words.size(); i++)
    {
        std::string query = words[i] + " ";
        TrieState state = trie->check_state(query);
        if (state != TrieState::CompleteNoChild && state != TrieState::CompleteHasChild)
        {
            success = false;
            std::cerr << "\nERROR: Word status is wrong!" << std::endl;
            std::cerr << "'" << query << "'" << std::endl;
            std::cerr << std::endl;
        }
    }

    if (!success)
    {
        throw(std::runtime_error("Tests failed!"));
    }
}

// =================================================================================================

void test_many_words(std::string path)
{
    bool success = true;
    size_t skip_query_words = 10;

    // Load word list
    std::ifstream f(path);
    std::string line;
    std::vector<std::string> words;
    while (std::getline(f, line))
    {
        words.push_back(line);
    }

    // Create a new trie
    Trie *trie = new Trie();

    // Insert all words
    auto begin_insert = std::chrono::high_resolution_clock::now();
    trie->insert_words(words);
    auto end_insert = std::chrono::high_resolution_clock::now();

    auto begin_query = std::chrono::high_resolution_clock::now();
    for (size_t i = 0; i < words.size(); i += skip_query_words)
    {
        std::string query = words[i] + " ";
        TrieState state = trie->check_state(query);

        if (state != TrieState::CompleteNoChild && state != TrieState::CompleteHasChild)
        {
            success = false;
            std::cerr << "\nERROR: Word status is wrong!" << std::endl;
            std::cerr << "'" << query << "'" << std::endl;
            std::cerr << std::endl;
        }
    }
    auto end_query = std::chrono::high_resolution_clock::now();

    double time_insert = std::chrono::duration_cast<std::chrono::nanoseconds>(end_insert - begin_insert).count();
    double time_query = std::chrono::duration_cast<std::chrono::nanoseconds>(end_query - begin_query).count();
    size_t num_query_words = (size_t)(words.size() / skip_query_words);

    std::cout << "Time to insert " << words.size() << " words: " << (time_insert / 1000000) << "ms" << std::endl;
    std::cout << "Time to query " << num_query_words << " words: " << (time_query / 1000000) << "ms";
    std::cout << " this are " << time_query / num_query_words << "ns per word" << std::endl;

    if (!success)
    {
        throw(std::runtime_error("Tests failed!"));
    }
}

// =================================================================================================

int main()
{
    test_tags();
    test_basic();
    test_search();
    test_special_words();
    test_incomplete_words();
    test_special_chars();

    test_many_words("/slungt/tests/data/words.txt");
    test_many_words("/slungt/tests/data/woerter-derewo-bearbeitet.txt");

    std::cout << "\nCompleted Trie tests\n";
    return 0;
}
