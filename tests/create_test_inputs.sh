#!/bin/bash

examples=(barrista riddles smartlights timersandsuch)
for xmpl in "${examples[@]}"
do
python3 ../preprocess/fullbuild_slu.py \
    --workdir "./outputs/$xmpl/" \
    --nlufile_path "./data/$xmpl.json" \
    --alphabet_path "./data/alphabet_en_chars.json"
done

# Convert inputs to json files for python tests
(
    g++ export_inputs.cpp -std=c++2a -Wall -o export_inputs.bin \
        && ./export_inputs.bin
)
