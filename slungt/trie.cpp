#include "trie.hpp"

// =================================================================================================
// =================================================================================================

Trie::Trie()
{
    this->root = new TrieNode("");
}

// =================================================================================================

void Trie::insert_word(const std::string &text)
{
    std::array<std::string, 3> splitted = this->extract_tags(text);

    // Remove any trailing or ending spaces
    std::string word = splitted[0];
    word.erase(word.find_last_not_of(" \n\r\t") + 1);
    word.erase(0, word.find_first_not_of(" \n\r\t"));

    // Add a single space at the end that each complete word ends with a space
    word = word + " ";

    this->root->insert(word, 0, splitted[1], splitted[2]);
}

void Trie::insert_words(const std::vector<std::string> &texts)
{
    for (size_t i = 0; i < texts.size(); i++)
    {
        this->insert_word(texts[i]);
    }
}

// =================================================================================================

TrieState Trie::check_state(const std::string &text) const
{
    return this->root->check_state(text, 0);
}

// =================================================================================================

std::vector<std::string> Trie::search(const std::string &text) const
{
    const auto &result = this->root->search(text, 0);
    std::vector<std::string> output;

    if (std::get<0>(result))
    {
        std::string text = std::get<1>(result);
        const auto &tags = std::get<2>(result);

        // Remove trailing space
        if (!text.empty())
            text.pop_back();

        for (size_t i = 0; i < tags.size(); i++)
        {
            std::string item = tags[i].first + text + tags[i].second;
            output.push_back(item);
        }
    }
    else
    {
        output.push_back("");
    }

    return output;
}

// =================================================================================================

std::array<std::string, 3> Trie::extract_tags(std::string text) const
{
    char tagBeginner = '[';
    char tagBreaker1 = ']';
    char tagBreaker2 = '>';

    std::string startTag = "";
    std::string endTag = "";

    if (!text.empty() && text[0] == tagBeginner)
    {
        startTag = "[";
        text.erase(0, 1);

        size_t endId = text.find(tagBreaker2);
        if (endId != std::string::npos)
        {
            // Subtract one because the full synonym tag is '->'
            endId -= 1;
        }
        else
        {
            endId = text.find(tagBreaker1);
        }

        endTag = text.substr(endId);
        text = text.substr(0, endId);
    }

    std::array<std::string, 3> output = {text, startTag, endTag};
    return output;
}

// =================================================================================================

int Trie::count_words() const
{
    return this->root->count_words();
}
