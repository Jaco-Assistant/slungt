#include <functional>
#include <iomanip>

#include "beam.hpp"
#include "utils.hpp"

// =================================================================================================
// =================================================================================================

std::ostream &operator<<(std::ostream &out, Beam const &beam)
{
    out << std::setprecision(5);
    out << "("
        << "text: '" << beam.text << "', "
        << "intent: '" << beam.intent << "', "
        << "score: " << beam.score << ", "
        << "partial_word: '" << beam.partial_word << "', "
        << "last_token: '" << beam.last_token << "', ";

    out << "new_words: [";
    if (!beam.new_words.empty())
    {
        out << beam.new_words[0];
        for (size_t i = 1; i < beam.new_words.size(); ++i)
        {
            out << ", " << beam.new_words[i];
        }
    }
    out << "], ";

    out << "beam_id: '" << beam.beam_id << "', "
        << "scores: [" << beam.logit_score << ", " << beam.trie_score << ", " << beam.lm_score
        << "])";

    return out;
}

// =================================================================================================
// =================================================================================================

Beam::Beam(std::string intent, size_t intent_id)
{
    this->text = "";
    this->intent = intent;
    this->intent_id = intent_id;

    this->score = 0.0f;
    this->logit_score = 0.0f;
    this->lm_score = 0.0f;
    this->trie_score = 0.0f;

    this->last_token = "";
    this->partial_word = "";

    this->beam_id = "0";
    this->tmp_word = "";
    this->new_words = {};

    this->found_entities = {};
    this->lm_state = nullptr;
}

// =================================================================================================

Beam::Beam(const Beam &other) = default;

// =================================================================================================

BeamResult Beam::create_result() const
{
    BeamResult result = {
        this->text,
        this->intent,
        this->score};
    return result;
}

// =================================================================================================

/*Adds new token to the partial-word, following CTC rules*/
void Beam::add_new_token(const std::string &next_token, float next_score)
{

    // If the new token is empty or the same as the last, the text doesn't change
    if (next_token.empty() || next_token == this->last_token)
    {
        this->last_token = next_token;
        this->logit_score += next_score;
        return;
    }

    char last_char = '\0';
    if (!this->partial_word.empty())
    {
        last_char = this->partial_word.back();
    }

    for (char c : next_token)
    {
        if (last_char == '\0' && c == ' ')
        {
            // Skip spaces at the beginning of a word
            continue;
        }
        if (last_char == ' ' && c == ' ')
        {
            // Skip duplicate whitespaces, which might be added by sentencepiece tokens
            continue;
        }

        this->partial_word += c;
        last_char = c;
    }

    this->last_token = next_token;
    this->logit_score += next_score;
}

// =================================================================================================

/*Calculates combined score from partial scores*/
void Beam::update_score()
{
    this->score = this->logit_score + this->trie_score + this->lm_score;
}

// =================================================================================================

/*Calculates combined score from partial scores*/
void Beam::include_new_words()
{
    for (std::string &word : this->new_words)
    {
        word = strip_text(word);
        if (!word.empty())
        {
            this->text += word + ' ';
        }
    }
    this->new_words.clear();
}

// =================================================================================================

/*Returns a hash key which can be used to merge equal beams*/
size_t Beam::get_beam_hash(const std::string &mode) const
{
    size_t klen = 0;
    klen += this->intent.length();
    klen += 1;
    klen += this->text.length();

    if (mode == "long" || mode == "full")
    {
        klen += 1;
        klen += this->partial_word.length();
        klen += 1;
        klen += this->last_token.length();
    }

    if (mode == "full")
    {
        klen += 1;
        klen += this->beam_id.length();
    }

    std::string key;
    key.reserve(klen);

    key += this->intent;
    key += '|';
    key += this->text;

    if (mode == "long" || mode == "full")
    {
        key += '|';
        key += this->partial_word;
        key += '|';
        key += this->last_token;
    }

    if (mode == "full")
    {
        key += '|';
        key += this->beam_id;
    }

    size_t khash = std::hash<std::string>()(key);
    return khash;
}

// =================================================================================================

/*Ends a beam by adding the remaining partial word and a penalty in case of a non-empty remainder*/
void Beam::finish(float penalty)
{
    if (!this->partial_word.empty())
    {
        this->trie_score += penalty;
    }
    this->partial_word = "";
    this->last_token = "";
    this->text = strip_text(this->text);
}
