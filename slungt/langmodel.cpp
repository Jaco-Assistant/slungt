#include "langmodel.hpp"
#include "lm/model.hh"

// =================================================================================================
// =================================================================================================

Langmodel::Langmodel(
    std::string lm_path,
    float alpha,
    float beta,
    bool log10_score)
{

    this->model.reset(lm::ngram::LoadVirtual(lm_path.c_str()));
    this->vocab = &model->BaseVocabulary();

    this->lm_path = lm_path;
    this->alpha = alpha;
    this->beta = beta;
    this->log10_score = log10_score;
}

// =================================================================================================

std::shared_ptr<lm::ngram::State> Langmodel::get_begin_state()
{
    std::shared_ptr<lm::ngram::State> in_state = std::make_shared<lm::ngram::State>();
    this->model->BeginSentenceWrite(in_state.get());
    return in_state;
}

// =================================================================================================

std::string Langmodel::get_end_token() const
{
    return "</s>";
}

// =================================================================================================

std::pair<float, std::shared_ptr<lm::ngram::State>> Langmodel::score_word(
    const std::string &word,
    const std::shared_ptr<lm::ngram::State> &in_state)
{
    std::shared_ptr<lm::ngram::State> out_state = std::make_shared<lm::ngram::State>();
    lm::WordIndex widx = this->vocab->Index(word);

    float lm_score = this->model->BaseScore(in_state.get(), widx, out_state.get());
    if (!this->log10_score)
        lm_score = lm_score * this->log_rebase;
    float score = this->alpha * lm_score + this->beta;

    std::pair<float, std::shared_ptr<lm::ngram::State>> result = std::make_pair(
        score, std::move(out_state));
    return result;
}
