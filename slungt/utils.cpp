#include "utils.hpp"

// =================================================================================================
// =================================================================================================

/*Remove leading and trailing whitespaces*/
std::string strip_text(const std::string &text)
{
    if (text.empty())
    {
        return text;
    }

    size_t start = text.find_first_not_of(" \n\t");
    size_t end = text.find_last_not_of(" \n\t");

    if (start == std::string::npos)
    {
        return "";
    }

    return text.substr(start, end - start + 1);
}
