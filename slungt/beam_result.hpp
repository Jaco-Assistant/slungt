#pragma once

#include <iostream>
#include <string>
#include <vector>

// =================================================================================================

struct BeamResult
{
    std::string text;
    std::string intent;
    float score;

    friend std::ostream &operator<<(std::ostream &out, BeamResult const &beam);
    std::string to_string() const;
};
