#include <sstream>

#include "intent.hpp"

// =================================================================================================
// =================================================================================================

std::string Entity::to_string() const
{
    std::ostringstream out;
    out << "{\"name\": \"" << this->name
        << "\", \"role\": \"" << this->role
        << "\", \"value\": \"" << this->value << "\"}";
    return out.str();
}

// =================================================================================================

std::string Intent::to_string() const
{
    std::ostringstream out;
    out << "{\"name\": \"" << this->name << "\","
        << "  \"text\": \"" << this->text << "\","
        << "  \"entities\": [";
    for (size_t i = 0; i < this->entities.size(); ++i)
    {
        out << this->entities[i];
        if (i < this->entities.size() - 1)
        {
            out << ", ";
        }
    }
    out << "]}";
    return out.str();
}
// =================================================================================================

std::ostream &operator<<(std::ostream &out, Entity const &item)
{
    out << item.to_string();
    return out;
}

// =================================================================================================

std::ostream &operator<<(std::ostream &out, Intent const &item)
{
    out << item.to_string();
    return out;
}
