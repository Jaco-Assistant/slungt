#pragma once

#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "beam.hpp"
#include "intent.hpp"
#include "langmodel.hpp"
#include "trie.hpp"

// =================================================================================================

class DecoderInternal
{
public:
    DecoderInternal(
        std::vector<std::string> alphabet,
        size_t max_beam_size,
        std::vector<std::vector<std::string>> vocabulary,
        std::vector<std::string> intents,
        std::vector<std::string> lm_paths,
        float lm_alpha,
        float lm_beta,
        float logit_weight,
        float token_min_logprob,
        size_t token_max_count,
        float beam_outlier_threshold,
        float unk_score_offset,
        size_t avg_token_len,
        float trie_split_offset,
        float entity_bonus,
        std::vector<float> intent_offsets,
        int blank_id);

    std::vector<BeamResult> decode(const std::vector<std::vector<float>> &ctc_tokens);
    std::string decode_greedy_text(const std::vector<std::vector<float>> &ctc_tokens);
    Intent extract_text2intent(const BeamResult &beam_result);

    void reset();
    void decode_step(const std::vector<float> &probs);
    std::vector<BeamResult> get_beam_results();
    std::vector<BeamResult> finalize();

private:
    std::vector<std::string> alphabet;
    size_t max_beam_size;
    std::vector<std::vector<std::string>> vocabulary;
    std::vector<std::string> intents;
    std::vector<std::string> lm_paths;
    float lm_alpha;
    float lm_beta;
    float logit_weight;
    float token_min_logprob;
    size_t token_max_count;
    float beam_outlier_threshold;
    float unk_score_offset;
    size_t avg_token_len;
    float trie_split_offset;
    float entity_bonus;
    std::vector<float> intent_offsets;
    int blank_id;

    std::vector<std::unique_ptr<Beam>> beams;
    std::vector<Trie> tries;
    std::vector<std::unique_ptr<Langmodel>> lms;

    bool using_lm;
    bool using_trie;
    bool multi_char_alphabet;

    void merge_equal_beams(std::string mode);
    void score_without_trie(std::unique_ptr<Beam> &beam);
    std::vector<std::unique_ptr<Beam>> score_with_trie(std::unique_ptr<Beam> &beam);
    std::vector<std::unique_ptr<Beam>> score_with_trie_multichar(std::unique_ptr<Beam> &beam);
    float get_penalty(const std::string &unknown_word);
    void score_new_words(std::unique_ptr<Beam> &beam);
    void score_end_state(std::unique_ptr<Beam> &beam);
};
