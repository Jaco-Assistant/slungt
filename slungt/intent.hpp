#pragma once

#include <iostream>
#include <string>
#include <vector>

// =================================================================================================

struct Entity
{
    std::string name;
    std::string role;
    std::string value;

    friend std::ostream &operator<<(std::ostream &out, Entity const &item);
    std::string to_string() const;
};

// =================================================================================================

struct Intent
{
    std::string name;
    std::string text;
    std::vector<Entity> entities;

    friend std::ostream &operator<<(std::ostream &out, Intent const &item);
    std::string to_string() const;
};
