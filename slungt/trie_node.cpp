#include "trie_node.hpp"

// =================================================================================================
// =================================================================================================

TrieNode::TrieNode(const std::string &key)
{
    this->key = key;

    this->isEndOfWord = false;
    this->tags = {};
}

// =================================================================================================

void TrieNode::insert(
    const std::string &text,
    const size_t index,
    const std::string &startTag,
    const std::string &endTag)
{
    if (index == text.size())
    {
        this->isEndOfWord = true;
        this->tags.push_back(std::make_pair(startTag, endTag));
        return;
    }

    const char ckey = text.at(index);
    auto it = this->children.find(ckey);

    // Recursively insert the rest of the text
    if (it != this->children.end())
    {
        const auto &child = it->second;
        child->insert(text, index + 1, startTag, endTag);
    }
    else
    {
        const std::string skey(1, ckey);
        TrieNode node_obj = TrieNode(skey);
        std::unique_ptr<TrieNode> node_ptr = std::make_unique<TrieNode>(std::move(node_obj));

        node_ptr->insert(text, index + 1, startTag, endTag);
        this->children.emplace(ckey, std::move(node_ptr));
    }
}

// =================================================================================================

TrieState TrieNode::check_state(const std::string &text, const size_t index) const
{
    if (index == text.size())
    {
        if (this->isEndOfWord)
        {
            if (!this->children.empty())
            {
                return TrieState::CompleteHasChild;
            }
            else
            {
                return TrieState::CompleteNoChild;
            }
        }
        else
        {
            return TrieState::Contained;
        }
    }

    const char ckey = text.at(index);
    auto it = this->children.find(ckey);

    if (it == this->children.end())
    {
        return TrieState::Missing;
    }
    else
    {
        const auto &child = it->second;
        TrieState cstate = child->check_state(text, index + 1);
        return cstate;
    }
}

// =================================================================================================

std::tuple<bool, std::string, std::vector<std::pair<std::string, std::string>>>
TrieNode::search(const std::string &text, const size_t index) const
{
    std::vector<std::pair<std::string, std::string>> tags;
    std::tuple<bool, std::string, std::vector<std::pair<std::string, std::string>>> output;

    if (index == text.size())
    {
        bool valid = false;
        if (this->isEndOfWord)
        {
            valid = true;
            tags = this->tags;
        }

        output = std::make_tuple(valid, this->key, tags);
        return output;
    }

    char key = text.at(index);
    auto it = this->children.find(key);
    if (it == this->children.end())
    {
        output = std::make_tuple(false, this->key, tags);
        return output;
    }
    else
    {
        const auto &child = it->second;
        output = child->search(text, index + 1);
        std::get<1>(output) = this->key + std::get<1>(output);
        return output;
    }
}

// =================================================================================================

int TrieNode::count_words() const
{
    int result = this->isEndOfWord ? 1 : 0;

    for (const auto &n : this->children)
    {
        result += n.second->count_words();
    }

    return result;
}
