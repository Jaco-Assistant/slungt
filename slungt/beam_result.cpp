#include <iomanip>
#include <sstream>

#include "beam_result.hpp"

// =================================================================================================
// =================================================================================================

std::string BeamResult::to_string() const
{
    std::ostringstream out;
    out << std::setprecision(5);
    out << "('" << this->text << "', " << this->intent << ", " << this->score << ")";
    return out.str();
}

// =================================================================================================

std::ostream &operator<<(std::ostream &out, BeamResult const &item)
{
    out << item.to_string();
    return out;
}
