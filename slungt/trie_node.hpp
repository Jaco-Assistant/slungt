#pragma once

#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "trie_state.hpp"

// =================================================================================================

class TrieNode
{
public:
    // The node's own key
    std::string key;

    // Hashmap for following nodes
    std::unordered_map<char, std::unique_ptr<TrieNode>> children;

    // Specifies if the node represents the end of a word
    bool isEndOfWord;

    // Tags that can be added before or after the content
    std::vector<std::pair<std::string, std::string>> tags;

    // If a word can be an entity and a normal word, use this flag to build all alternatives
    // The ngram model is afterwards used to decide between the options
    bool isTagOptional;

    // Initialize a new node
    TrieNode(const std::string &key);

    // Inserts text recursively into child nodes
    void insert(const std::string &text, const size_t index, const std::string &startTag, const std::string &endTag);

    // Check if key exists in child nodes and return the node's state
    TrieState check_state(const std::string &text, const size_t index) const;

    // Return word including tags if existing in child nodes
    // Result: [valid, text, list of (tagStart, tagEnd)]
    std::tuple<bool, std::string, std::vector<std::pair<std::string, std::string>>>
    search(const std::string &text, const size_t index) const;

    // Count words in child nodes
    int count_words() const;
};
