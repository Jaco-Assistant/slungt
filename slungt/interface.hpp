#pragma once

#include <string>
#include <vector>

#include "beam_result.hpp"
#include "intent.hpp"

// =================================================================================================

// Forward declaration of the class, that swig does try to parse all its dependencies.
class DecoderInternal;

// =================================================================================================

class Decoder
{
public:
    /**
     * Decoder to run beam-search on ctc-style predictions.
     *
     *
     * @param lm_alpha Parameter for length normalization.
     * A lower value penalizes long sentences, a higher reduces the costs to add additional words.
     * @param lm_beta Parameter for word bonus.
     * @param logit_weight Parameter for token probability weight.
     * @param token_min_logprob Minimum log-probability for a token to be considered.
     * @param token_max_count Maximum number of tokens to consider in each step.
     * @param beam_outlier_threshold Threshold for beam pruning.
     * @param unk_score_offset Offset for unknown tokens.
     * @param avg_token_len Average token length.
     * @param trie_split_offset A small positive offset to prefer word combinations
     * over splitted words if both options are existing in the trie.
     * @param entity_bonus A small positive offset to reward finding of entities.
     * @param intent_offsets An offset to prefer or penalize certain intents.
     * @param blank_id Index of the ctc-blank token, negative values are counted from back.
     */
    Decoder(
        std::vector<std::string> alphabet,
        size_t max_beam_size = 128,
        std::vector<std::vector<std::string>> vocabulary = {},
        std::vector<std::string> intents = {},
        std::vector<std::string> lm_paths = {},
        float lm_alpha = 1.0,
        float lm_beta = 1.0,
        float logit_weight = 1.0,
        float token_min_logprob = -10.0,
        size_t token_max_count = 64,
        float beam_outlier_threshold = -20.0,
        float unk_score_offset = -10.0,
        size_t avg_token_len = 6,
        float trie_split_offset = 0.01,
        float entity_bonus = 0.001,
        std::vector<float> intent_offsets = {},
        int blank_id = -1);

    /**
     * Decode a complete prediction.
     *
     * Alternatively, to stream the decoding process,
     * call reset(), decode_step() for each timestep, and finalize().
     *
     * @param probs List of timesteps, each containing per-token probabilities in softmax format.
     * @return List of beams {text, score, token of each timestep}, sorted by score.
     */
    std::vector<BeamResult> decode(std::vector<std::vector<float>> probs);

    /**
     * Calculate the greedy text from the probabilities.
     *
     *
     * @param probs List of timesteps, each containing per-token probabilities in softmax format.
     * @return Greed decoded text.
     */
    std::string decode_greedy_text(std::vector<std::vector<float>> probs);

    // Create a new beam collection.
    void reset();

    /**
     * Send the next timestep to the decoder.
     *
     *
     * @param probs A timestep of per-token probabilities in softmax format.
     * @return List of current beams {text, score, token of each timestep}, sorted by score.
     */
    std::vector<BeamResult> decode_step(std::vector<float> probs);

    /**
     * Finalize a streamed decoding process.
     *
     *
     * @return List of finished beams {text, score, token of each timestep}, sorted by score.
     */
    std::vector<BeamResult> finalize();

    /**
     * Extract the intent from a beam result.
     *
     *
     * @param beam_result A beam result.
     * @return The intent.
     */
    Intent extract_text2intent(const BeamResult beam_result);

private:
    DecoderInternal *decoder;
};
