#include <string>
#include <memory>

#include "lm/model.hh"

// =================================================================================================

class Langmodel
{
public:
    Langmodel(
        std::string lm_path,
        float alpha = 1.0,
        float beta = 1.0,
        bool log10_score = true);

    Langmodel(const Langmodel &) = delete;
    Langmodel &operator=(const Langmodel &) = delete;

    /**
     * Calculate score for the word depending on a previous state.
     *
     * @return pair(score, state_out): Score of new word and the new LM state.
     */
    std::pair<float, std::shared_ptr<lm::ngram::State>> score_word(
        const std::string &word,
        const std::shared_ptr<lm::ngram::State> &in_state);

    std::shared_ptr<lm::ngram::State> get_begin_state();
    std::string get_end_token() const;

private:
    std::unique_ptr<lm::base::Model> model;
    const lm::base::Vocabulary *vocab;

    std::string lm_path;
    float alpha;
    float beta;
    bool log10_score;

    const float log_rebase = 1.0 / (std::log(std::exp(1.0)) / std::log(10.0));
};
