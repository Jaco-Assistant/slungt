#pragma once

// =================================================================================================

enum class TrieState
{
    // Not contained in the trie
    Missing,
    // Contained but not a complete word
    Contained,
    // Complete word and end node
    CompleteNoChild,
    // Complete word, but could also be part of another word
    CompleteHasChild,
};
