#pragma once

#include <string>

// =================================================================================================

std::string strip_text(const std::string &text);
