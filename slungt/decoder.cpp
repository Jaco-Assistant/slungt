#include <algorithm>
#include <chrono>
#include <cmath>
#include <iostream>
#include <numeric>
#include <regex>
#include <unordered_map>

#include "decoder.hpp"
#include "trie_state.hpp"
#include "utils.hpp"

// =================================================================================================
// =================================================================================================

// Return the sum of two probabilities in log scale
float sum_log_probs(float s1, float s2)
{
    if (s1 > s2)
    {
        return s1 + std::log1p(std::exp(s2 - s1));
    }
    else
    {
        return s2 + std::log1p(std::exp(s1 - s2));
    }
}

// =================================================================================================

/*Extracts words from a text that are followed by a space*/
std::vector<std::string> split_words_with_space(const std::string &text, bool keep_ending_space)
{
    std::vector<std::string> words;
    std::istringstream iss(text);
    std::string word;
    while (std::getline(iss, word, ' '))
    {
        if (!iss.eof() && !word.empty())
        {
            if (keep_ending_space)
            {
                word += ' ';
            }
            words.push_back(word);
        }
    }
    return words;
}

// =================================================================================================
// =================================================================================================

DecoderInternal::DecoderInternal(
    std::vector<std::string> alphabet,
    size_t max_beam_size,
    std::vector<std::vector<std::string>> vocabulary,
    std::vector<std::string> intents,
    std::vector<std::string> lm_paths,
    float lm_alpha,
    float lm_beta,
    float logit_weight,
    float token_min_logprob,
    size_t token_max_count,
    float beam_outlier_threshold,
    float unk_score_offset,
    size_t avg_token_len,
    float trie_split_offset,
    float entity_bonus,
    std::vector<float> intent_offsets,
    int blank_id)
{
    auto ttime = std::chrono::high_resolution_clock::now();
    std::chrono::duration<float> elapsed;

    this->alphabet = alphabet;
    this->alphabet.push_back("");

    this->max_beam_size = max_beam_size;
    this->intents = intents;
    this->lm_alpha = lm_alpha;
    this->lm_beta = lm_beta;
    this->logit_weight = logit_weight;
    this->token_min_logprob = token_min_logprob;
    this->token_max_count = token_max_count;
    this->beam_outlier_threshold = beam_outlier_threshold;
    this->unk_score_offset = unk_score_offset;
    this->avg_token_len = avg_token_len;
    this->trie_split_offset = trie_split_offset;
    this->entity_bonus = entity_bonus;

    if (intents.size() > max_beam_size)
    {
        throw std::invalid_argument("Use a higher beam-size!");
    }
    else if (intents.size() * 3 > max_beam_size)
    {
        std::cout << "You probably want to use a higher beam-size!" << std::endl;
    }

    if ((intents.size() > 0 && !(intents.size() == vocabulary.size())) ||
        (lm_paths.size() > 0 && !(intents.size() == lm_paths.size())))
    {
        throw std::invalid_argument("Number of intents, LMs or vocabularies not matching!");
    }

    this->using_trie = false;
    this->using_lm = false;

    for (const auto &vocab : vocabulary)
    {
        Trie word_trie;
        word_trie.insert_words(vocab);
        this->tries.push_back(std::move(word_trie));
        this->using_trie = true;
    }

    for (const auto &lm_path : lm_paths)
    {
        std::unique_ptr<Langmodel> lm = std::make_unique<Langmodel>(
            lm_path, this->lm_alpha, this->lm_beta, false);
        this->lms.push_back(std::move(lm));
        this->using_lm = true;
    }

    if (this->using_lm && !this->using_trie)
    {
        throw std::invalid_argument("When using a language model, a vocabulary is required!");
    }

    if (intent_offsets.size() > 0 && !(intents.size() == intent_offsets.size()))
    {
        throw std::invalid_argument("Number of intents and offsets not matching!");
    }

    if (intent_offsets.size() == 0 && intents.size() > 0)
    {
        for (size_t i = 0; i < intents.size(); ++i)
        {
            this->intent_offsets.push_back(0.0);
        }
    }
    else
    {
        this->intent_offsets = intent_offsets;
    }

    // Replace sentencepiece-style space labels in the alphabet with a normal space
    for (size_t i = 0; i < this->alphabet.size(); ++i)
    {
        std::string &token = this->alphabet[i];
        token = std::regex_replace(token, std::regex("▁"), " ");
        token = std::regex_replace(token, std::regex("<unk>"), " ");
    }

    // Check if the alphabet contains multi-character tokens
    this->multi_char_alphabet = false;
    for (const std::string &token : this->alphabet)
    {
        if (token.length() > 1)
        {
            this->multi_char_alphabet = true;
            break;
        }
    }

    // Calculate correct blank-id
    if (blank_id >= 0)
    {
        this->blank_id = blank_id;
    }
    else
    {
        this->blank_id = this->alphabet.size() + blank_id;
    }

    // Print time taken
    elapsed = std::chrono::high_resolution_clock::now() - ttime;
    std::cout << "Time to load decoder: " << elapsed.count() << "s" << std::endl;
}

// =================================================================================================

std::string DecoderInternal::decode_greedy_text(const std::vector<std::vector<float>> &ctc_tokens)
{
    // Get the most likely character for each timestep
    std::vector<int> values;
    for (const auto &row : ctc_tokens)
    {
        auto max_it = std::max_element(row.begin(), row.end());
        values.push_back(std::distance(row.begin(), max_it));
    }

    // Merge repeated characters
    std::vector<int> merged;
    if (!values.empty())
    {
        merged.push_back(values[0]);
        for (size_t i = 1; i < values.size(); ++i)
        {
            if (values[i] != merged.back())
            {
                merged.push_back(values[i]);
            }
        }
    }

    // Remove blanks
    merged.erase(std::remove(merged.begin(), merged.end(), this->blank_id), merged.end());

    // Merge to text
    std::string gd_text;
    for (int v : merged)
    {
        gd_text += this->alphabet[v];
    }

    // Replace starting and ending spaces
    gd_text = strip_text(gd_text);

    return gd_text;
}

// =================================================================================================

void DecoderInternal::reset()
{
    this->beams.clear();

    if (this->intents.size() > 0)
    {
        for (size_t i = 0; i < this->intents.size(); ++i)
        {
            Beam beam_obj = Beam(this->intents[i], i);
            std::unique_ptr<Beam> beam = std::make_unique<Beam>(std::move(beam_obj));
            if (this->using_lm)
            {
                beam->lm_state = this->lms[i]->get_begin_state();
            }
            this->beams.push_back(std::move(beam));
        }
    }
    else
    {
        Beam beam_obj = Beam("", 0);
        std::unique_ptr<Beam> beam = std::make_unique<Beam>(std::move(beam_obj));
        if (this->using_lm)
        {
            beam->lm_state = this->lms[0]->get_begin_state();
        }
        this->beams.push_back(std::move(beam));
    }
}

// =================================================================================================

std::vector<BeamResult> DecoderInternal::get_beam_results()
{
    std::vector<BeamResult> results;
    for (const std::unique_ptr<Beam> &beam : this->beams)
    {
        BeamResult result = beam->create_result();
        results.push_back(std::move(result));
    }
    return results;
}

// =================================================================================================

std::vector<BeamResult> DecoderInternal::decode(const std::vector<std::vector<float>> &ctc_tokens)
{
    auto ttime = std::chrono::high_resolution_clock::now();
    std::chrono::duration<float> elapsed;

    // Reset decoder state
    this->reset();

    // Now decode the probabilities step by step
    for (size_t i = 0; i < ctc_tokens.size(); ++i)
    {
        this->decode_step(ctc_tokens[i]);
    }

    // Finalize the decoding
    const std::vector<BeamResult> &results = this->finalize();

    // Print time taken
    elapsed = std::chrono::high_resolution_clock::now() - ttime;
    std::cout << "Time to decode probs: " << elapsed.count() << "s" << std::endl;

    return results;
}

// =================================================================================================

void DecoderInternal::decode_step(const std::vector<float> &probs)
{
    // Ensure no probability is zero or close to it
    std::vector<float> one_probs = probs;
    for (size_t i = 0; i < one_probs.size(); ++i)
    {
        one_probs[i] = std::max(1e-15f, one_probs[i]);
    }

    // Ensure that all timesteps have a probability of exactly one
    float psum = std::accumulate(one_probs.begin(), one_probs.end(), 0.0f);
    float inv_p = 1.0f / psum;
    for (size_t i = 0; i < one_probs.size(); ++i)
    {
        one_probs[i] *= inv_p;
    }

    // Convert from normal probabilities to log probabilities
    std::vector<float> log_probs = probs;
    for (size_t i = 0; i < log_probs.size(); ++i)
    {
        log_probs[i] = std::log(log_probs[i]);
    }

    // Filter tokens below minimal threshold, but keep best match even if it's below it
    size_t max_idx = 0;
    float max_prob = log_probs[0];
    std::vector<size_t> token_ids;
    for (size_t i = 0; i < log_probs.size(); ++i)
    {
        float prob = log_probs[i];
        if (prob >= this->token_min_logprob)
        {
            token_ids.push_back(i);
        }
        if (prob >= max_prob)
        {
            max_prob = prob;
            max_idx = i;
        }
    }
    if (token_ids.size() == 0)
    {
        token_ids.push_back(max_idx);
    }

    // Make a list of token-probability pairs for the next step
    std::vector<std::pair<size_t, float>> token_prob_pairs;
    token_prob_pairs.reserve(token_ids.size());
    for (const int tid : token_ids)
    {
        float wprob = this->logit_weight * log_probs[tid];
        token_prob_pairs.emplace_back(tid, wprob);
    }

    // If there are too much tokens, keep only the best ones, to prevent combinatorial explosions
    if (token_prob_pairs.size() > this->token_max_count)
    {
        std::partial_sort(
            token_prob_pairs.begin(), token_prob_pairs.begin() + this->token_max_count,
            token_prob_pairs.end(),
            [](const std::pair<size_t, float> &a, const std::pair<size_t, float> &b)
            { return a.second > b.second; });

        token_prob_pairs.resize(this->token_max_count);
    }

    // Calculate a minimum cutoff score below which new beams are not created
    // because they would later be pruned anyway
    float min_cutoff_score = std::numeric_limits<float>::infinity();
    for (const std::unique_ptr<Beam> &beam : this->beams)
    {
        if (beam->score < min_cutoff_score)
        {
            min_cutoff_score = beam->score;
        }
    }
    min_cutoff_score += log_probs[this->blank_id];
    if (this->using_lm)
    {
        min_cutoff_score -= std::max(0.0f, this->lm_beta);
    }
    if (this->using_trie)
    {
        min_cutoff_score -= std::max(0.0f, this->entity_bonus);
        min_cutoff_score -= std::max(0.0f, this->trie_split_offset);
    }

    // Create all combinations of current beams with the next token
    std::vector<std::unique_ptr<Beam>> new_beams;
    new_beams.reserve(this->beams.size() * token_ids.size());
    for (const std::unique_ptr<Beam> &beam : this->beams)
    {
        for (const auto &pair : token_prob_pairs)
        {
            if (this->beams.size() < this->max_beam_size ||
                beam->score + pair.second > min_cutoff_score)
            {
                std::unique_ptr<Beam> new_beam = std::make_unique<Beam>(*beam);
                new_beam->add_new_token(alphabet[pair.first], pair.second);
                new_beams.push_back(std::move(new_beam));
            }
        }
    }
    this->beams = std::move(new_beams);

    // Merge equal beams
    this->merge_equal_beams("combine");

    // Check if the partial word contains complete words
    if (!this->using_trie)
    {
        for (std::unique_ptr<Beam> &beam : this->beams)
        {
            this->score_without_trie(beam);
        }
    }
    else
    {
        std::vector<std::unique_ptr<Beam>> evaluated_beams;
        for (std::unique_ptr<Beam> &beam : this->beams)
        {
            std::vector<std::unique_ptr<Beam>> trie_beams = this->score_with_trie(beam);
            for (std::unique_ptr<Beam> &trie_beam : trie_beams)
            {
                evaluated_beams.push_back(std::move(trie_beam));
            }
        }
        this->beams = std::move(evaluated_beams);
    }

    // Score the new words
    if (this->using_trie || this->using_lm)
    {
        for (std::unique_ptr<Beam> &beam : this->beams)
        {
            this->score_new_words(beam);
        }
    }

    // Update beam scores
    for (const std::unique_ptr<Beam> &beam : this->beams)
    {
        beam->update_score();
    }

    // Remove beam outliers which have a much worse probability than the current best beam
    float max_score = -std::numeric_limits<float>::infinity();
    for (const std::unique_ptr<Beam> &beam : this->beams)
    {
        if (beam->score > max_score)
        {
            max_score = beam->score;
        }
    }
    float min_score = max_score + this->beam_outlier_threshold;
    auto removeIt = std::remove_if(
        this->beams.begin(), this->beams.end(),
        [min_score](const std::unique_ptr<Beam> &beam)
        {
            return beam->score <= min_score;
        });
    this->beams.erase(removeIt, this->beams.end());

    // Prune beams by keeping only the best ones if there are too many
    if (this->beams.size() > max_beam_size)
    {
        std::partial_sort(
            this->beams.begin(), this->beams.begin() + max_beam_size, this->beams.end(),
            [](const std::unique_ptr<Beam> &a, const std::unique_ptr<Beam> &b)
            {
                return a->score > b->score;
            });

        this->beams.resize(max_beam_size);
    }

    // Add new words to the beam's text
    for (const std::unique_ptr<Beam> &beam : this->beams)
    {
        beam->include_new_words();
    }

    // Keep the best beam of the beams with equal beam-ids
    // The split is caused by the trie based splitting and might result in the same content with
    // different beam-ids if the last character was repeated and the detected word can be continued.
    this->merge_equal_beams("best");
}

// =================================================================================================

std::vector<BeamResult> DecoderInternal::finalize()
{
    // Add an additional space probability at the end to ensure the last word completion
    size_t ntoks = this->alphabet.size();
    std::vector<float> extra_space(ntoks, 0);
    size_t space_idx = std::distance(this->alphabet.begin(),
                                     std::find(this->alphabet.begin(), this->alphabet.end(), " "));
    extra_space[space_idx] = 1.0;
    this->decode_step(extra_space);

    // Penalize unfinished beams and drop incomplete words
    // Don't merge them afterwards, because the combination step doesn't merge the penalty correctly
    for (const std::unique_ptr<Beam> &beam : this->beams)
    {
        beam->finish(this->unk_score_offset);
    }

    if (this->using_lm)
    {
        // Rescore with end-state
        for (std::unique_ptr<Beam> &beam : this->beams)
        {
            this->score_end_state(beam);
        }
    }

    // Update scores
    for (const std::unique_ptr<Beam> &beam : this->beams)
    {
        beam->update_score();
    }

    // Filter beams and keep the best option of each beam path
    // This time without considering the beam-ids, but only using text and intent values
    this->merge_equal_beams("last");

    // Apply intent offsets
    if (this->intent_offsets.size() > 0)
    {
        for (const std::unique_ptr<Beam> &beam : this->beams)
        {
            beam->score += this->intent_offsets[beam->intent_id];
        }
    }

    // Sort beams by score
    std::sort(this->beams.begin(), this->beams.end(),
              [](const std::unique_ptr<Beam> &a, const std::unique_ptr<Beam> &b)
              { return a->score > b->score; });

    // Get output beams
    std::vector<BeamResult> results = std::move(this->get_beam_results());

    return results;
}

// =================================================================================================

void DecoderInternal::merge_equal_beams(std::string mode)
{
    bool combine_scores;
    std::string hmode;
    if (mode == "last")
    {
        combine_scores = false;
        hmode = "base";
    }
    else if (mode == "best")
    {
        combine_scores = false;
        hmode = "long";
    }
    else
    {
        combine_scores = true;
        hmode = "full";
    }

    // Update scores or best beams and mark duplicates
    std::unordered_map<size_t, size_t> seen_indices;
    seen_indices.reserve(this->beams.size());
    for (size_t i = 0; i < this->beams.size(); ++i)
    {
        const size_t bkey = this->beams[i]->get_beam_hash(hmode);

        auto it = seen_indices.find(bkey);
        if (it != seen_indices.end())
        {
            if (combine_scores)
            {
                this->beams[it->second]->logit_score = sum_log_probs(
                    this->beams[it->second]->logit_score, this->beams[i]->logit_score);
            }
            else
            {
                if (this->beams[i]->score > this->beams[it->second]->score)
                {
                    it->second = i;
                }
            }
        }
        else
        {
            seen_indices.emplace(bkey, i);
        }
    }

    // Remove duplicates
    std::vector<std::unique_ptr<Beam>> unique_beams;
    unique_beams.reserve(seen_indices.size());
    for (const auto &pair : seen_indices)
    {
        unique_beams.push_back(std::move(this->beams[pair.second]));
    }
    this->beams = std::move(unique_beams);
}

// =================================================================================================

void DecoderInternal::score_without_trie(std::unique_ptr<Beam> &beam)
{
    size_t spacePos = beam->partial_word.find(' ');
    if (spacePos != std::string::npos)
    {
        // Split partial word along spaces if there are any
        do
        {
            beam->new_words.push_back(beam->partial_word.substr(0, spacePos));
            beam->partial_word = beam->partial_word.substr(spacePos + 1);
            spacePos = beam->partial_word.find(' ');
        } while (spacePos != std::string::npos);
    }
}

// =================================================================================================

std::vector<std::unique_ptr<Beam>> DecoderInternal::score_with_trie(std::unique_ptr<Beam> &beam)
{
    std::vector<std::unique_ptr<Beam>> new_beams;

    // Check if the partial word is contained in the trie
    TrieState status1 = this->tries[beam->intent_id].check_state(beam->partial_word);

    if (status1 == TrieState::Contained)
    {
        // Word not yet finished, keep it and continue
        new_beams.push_back(std::move(beam));
    }
    else if (status1 == TrieState::CompleteNoChild || status1 == TrieState::CompleteHasChild)
    {
        // The word is complete, but could also be part of another word
        const std::vector<std::string> &words = this->tries[beam->intent_id].search(beam->partial_word);

        // Create a new beam for each word option so they are filtered by regular scoring.
        // Since the beams with their current path-probabilities can be duplicated here, they
        // need to get individual beam-ids to prevent merging them again, which would create
        // wrong combined path-probabilities.
        for (size_t i = 0; i < words.size(); ++i)
        {
            Beam nbeam_obj = Beam(*beam);
            std::unique_ptr<Beam> nbeam = std::make_unique<Beam>(std::move(nbeam_obj));
            nbeam->partial_word.clear();
            nbeam->beam_id += "-" + std::to_string(i + 1);
            nbeam->new_words = {words[i]};
            new_beams.push_back(std::move(nbeam));
        }

        if (status1 == TrieState::CompleteHasChild)
        {
            // Keep current unfinished word in one beam
            beam->beam_id += "-0";
            beam->trie_score += this->trie_split_offset;
            new_beams.push_back(std::move(beam));
        }
    }
    else
    {
        if (!this->multi_char_alphabet)
        {
            if (!beam->partial_word.empty())
            {
                float penalty = this->get_penalty(beam->partial_word);
                beam->trie_score += penalty;

                if (beam->partial_word.back() == ' ')
                {
                    // Extracting the unknown word if present
                    beam->partial_word.pop_back();
                    beam->new_words = {beam->partial_word};
                    beam->partial_word.clear();
                }

                new_beams.push_back(std::move(beam));
            }
        }
        else
        {
            // Multichar tokens can have multiple subwords which makes scoring much more complex
            std::vector<std::unique_ptr<Beam>> mc_beams = this->score_with_trie_multichar(beam);
            new_beams = std::move(mc_beams);
        }
    }

    return new_beams;
}

// =================================================================================================

std::vector<std::unique_ptr<Beam>> DecoderInternal::score_with_trie_multichar(
    std::unique_ptr<Beam> &beam)
{
    std::vector<std::unique_ptr<Beam>> new_beams;
    size_t space_count = std::count(beam->partial_word.begin(), beam->partial_word.end(), ' ');

    if (space_count == 0)
    {
        // Word not yet finished, keep it but add a score penalty
        float penalty = this->get_penalty(beam->partial_word);
        beam->trie_score += penalty;
        new_beams.push_back(std::move(beam));
    }
    else if (beam->partial_word.back() == ' ' && space_count == 1)
    {
        // Word is unknown but there is only one space at the end, so it was already tested.
        // Add the unknown word with a penalty.
        beam->partial_word.pop_back();
        beam->new_words = {beam->partial_word};
        float penalty = this->get_penalty(beam->partial_word);
        beam->partial_word.clear();
        beam->trie_score += penalty;
        new_beams.push_back(std::move(beam));
    }
    else
    {
        // Sentencepiece tokens can start with a space and include some additional
        // characters, with the result that the partial word will be unknown. Split
        // the partial word into parts ending with a space and test them again.
        // Consider for example: ["soy_milk_and", "soy_milk_nd", "soy_milds_nd", "and_soy", "soy_milds",
        //   "so_", "milk_widh", "medium_roast_and", "medium_roast_nd", "medium_rd", "medium_and_",
        //   "a_bit_of_sugar_and", , "a_bit_of_nd"]

        // Extract completed words
        const std::vector<std::string> &parts = split_words_with_space(beam->partial_word, true);

        // Initialize with the original beam
        beam->tmp_word.clear();
        std::vector<std::unique_ptr<Beam>> split_beams;
        split_beams.push_back(std::move(beam));

        // Iterate over completed words
        for (size_t i = 0; i < parts.size(); ++i)
        {
            std::vector<std::unique_ptr<Beam>> ns_beams;

            for (size_t j = 0; j < split_beams.size(); ++j)
            {
                std::unique_ptr<Beam> current_beam = std::move(split_beams[j]);
                current_beam->tmp_word += parts[i];

                TrieState status2 = this->tries[current_beam->intent_id].check_state(current_beam->tmp_word);

                if (status2 == TrieState::Contained)
                {
                    std::string rest = current_beam->partial_word.substr(current_beam->tmp_word.size());
                    bool has_space = (rest.find(' ') != std::string::npos);

                    if (has_space)
                    {
                        // Word not yet finished, keep as prefix and continue
                        ns_beams.push_back(std::move(current_beam));
                    }
                    else
                    {
                        // Word not yet finished, but the combined part was found unknown before
                        // Split the word apart and keep the parts but add a penalty
                        current_beam->partial_word.erase(0, current_beam->tmp_word.size());
                        current_beam->new_words = split_words_with_space(current_beam->tmp_word, false);
                        float penalty = this->get_penalty(current_beam->tmp_word);
                        current_beam->trie_score += penalty;
                        current_beam->tmp_word.clear();
                        ns_beams.push_back(std::move(current_beam));
                    }
                }
                else if (status2 == TrieState::CompleteNoChild || status2 == TrieState::CompleteHasChild)
                {
                    const std::vector<std::string> &words = this->tries[current_beam->intent_id].search(
                        current_beam->tmp_word);

                    // Create new beams for each returned word
                    for (const std::string &word : words)
                    {
                        Beam nbeam_obj = Beam(*current_beam);
                        std::unique_ptr<Beam> nbeam = std::make_unique<Beam>(std::move(nbeam_obj));
                        nbeam->beam_id += "-" + std::to_string(i + 1);
                        nbeam->partial_word.erase(0, nbeam->tmp_word.size());
                        nbeam->tmp_word.clear();
                        nbeam->new_words = {word};
                        ns_beams.push_back(std::move(nbeam));
                    }

                    if (status2 == TrieState::CompleteHasChild)
                    {
                        // Keep current unfinished word in one beam
                        current_beam->beam_id += "-0";
                        current_beam->trie_score += this->trie_split_offset;
                        ns_beams.push_back(std::move(current_beam));
                    }
                }
                else
                {
                    // Word not known, keep it but add a score penalty
                    float penalty = this->get_penalty(current_beam->tmp_word);
                    current_beam->partial_word.erase(0, current_beam->tmp_word.size());
                    current_beam->new_words = {current_beam->tmp_word};
                    current_beam->trie_score += penalty;
                    current_beam->tmp_word.clear();
                    ns_beams.push_back(std::move(current_beam));
                }
            }
            split_beams = std::move(ns_beams);
        }

        // Check the remaining part without a space
        for (auto &current_beam : split_beams)
        {
            if (!current_beam->partial_word.empty())
            {
                TrieState status3 = this->tries[current_beam->intent_id].check_state(
                    current_beam->partial_word);
                if (status3 != TrieState::Contained)
                {
                    float penalty = this->get_penalty(current_beam->partial_word);
                    current_beam->trie_score += penalty;
                }
            }
        }
        new_beams = std::move(split_beams);
    }

    return new_beams;
}

// =================================================================================================

float DecoderInternal::get_penalty(const std::string &unknown_word)
{
    float penalty = this->unk_score_offset;
    size_t nchars = unknown_word.length();

    if (nchars > this->avg_token_len)
    {
        float factor = static_cast<float>(nchars) / this->avg_token_len;
        penalty = penalty * factor;
    }

    return penalty;
}

// =================================================================================================

void DecoderInternal::score_new_words(std::unique_ptr<Beam> &beam)
{
    for (const std::string &word : beam->new_words)
    {
        std::string modified_word = strip_text(word);
        if (modified_word.empty())
        {
            continue;
        }

        size_t bracket_pos = modified_word.find(']');
        if (bracket_pos != std::string::npos)
        {
            modified_word.erase(0, bracket_pos + 1);

            if (beam->found_entities.find(modified_word) == beam->found_entities.end())
            {
                beam->found_entities.insert(modified_word);
                beam->trie_score = beam->trie_score + this->entity_bonus;
            }
        }

        if (this->using_lm)
        {
            std::pair<float, std::shared_ptr<lm::ngram::State>> result =
                this->lms[beam->intent_id]->score_word(modified_word, beam->lm_state);
            beam->lm_state = std::move(result.second);
            beam->lm_score += result.first;
        }
    }
}

// =================================================================================================

void DecoderInternal::score_end_state(std::unique_ptr<Beam> &beam)
{
    std::pair<float, std::shared_ptr<lm::ngram::State>> result =
        this->lms[beam->intent_id]->score_word(
            this->lms[beam->intent_id]->get_end_token(), beam->lm_state);
    beam->lm_state = std::move(result.second);
    beam->lm_score += result.first;
}

// =================================================================================================

Intent DecoderInternal::extract_text2intent(const BeamResult &beam_result)
{
    Intent intent;
    intent.name = beam_result.intent;

    std::string sentence = beam_result.text;
    std::regex splitter(R"( (?![^(]*\))(?![^[]*\]))");
    std::vector<std::string> components(
        std::sregex_token_iterator(sentence.begin(), sentence.end(), splitter, -1),
        std::sregex_token_iterator());

    std::stringstream spoken_text;
    for (const std::string &component : components)
    {
        if (component[0] == '[')
        {
            // Slot
            Entity slot;
            size_t split_pos = component.find("](");
            std::string value = component.substr(1, split_pos - 1);
            std::string name = component.substr(split_pos + 2);
            name = name.substr(0, name.size() - 1);

            if (name.find('?') != std::string::npos)
            {
                // Slot with role
                size_t role_pos = name.find('?');
                slot.name = name.substr(0, role_pos);
                slot.role = name.substr(role_pos + 1);
            }
            else
            {
                slot.name = name;
            }

            size_t synonymPos = value.find("->");
            if (synonymPos != std::string::npos)
            {
                // Value with synonym
                slot.value = value.substr(synonymPos + 2);
                value = value.substr(0, synonymPos);
            }
            else
            {
                slot.value = value;
            }

            spoken_text << value << " ";
            intent.entities.push_back(slot);
        }
        else
        {
            // Normal word
            spoken_text << strip_text(component) << " ";
        }
    }

    std::string text = spoken_text.str();
    intent.text = text.substr(0, text.size() - 1);
    return intent;
}
