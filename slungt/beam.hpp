#pragma once

#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <set>
#include <lm/state.hh>

#include "beam_result.hpp"

// =================================================================================================

class Beam
{
public:
    Beam(std::string intent = "", size_t intent_id = 0);
    Beam(const Beam &org);
    friend std::ostream &operator<<(std::ostream &out, Beam const &beam);

    BeamResult create_result() const;
    void add_new_token(const std::string &next_token, float next_score);
    void update_score();
    void include_new_words();
    size_t get_beam_hash(const std::string &mode) const;
    void finish(float penalty);

    std::string text;
    std::string intent;
    size_t intent_id;

    float score;
    float logit_score;
    float lm_score;
    float trie_score;

    std::string last_token;
    std::string partial_word;

    std::string beam_id;
    std::string tmp_word;
    std::vector<std::string> new_words;

    std::set<std::string> found_entities;
    std::shared_ptr<lm::ngram::State> lm_state;
};
