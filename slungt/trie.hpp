#pragma once

#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

#include "trie_state.hpp"
#include "trie_node.hpp"

// =================================================================================================

class Trie
{
public:
    // Initialize a new trie
    Trie();

    // Insert a single word into the trie
    void insert_word(const std::string &text);

    // Insert a list of words into the trie
    void insert_words(const std::vector<std::string> &texts);

    // Return if text exist as a partial or complete word in trie and return the node's state
    TrieState check_state(const std::string &text) const;

    // Return word including tags if existing in child nodes
    // Result: Either one or more strings depending on the use of tags
    std::vector<std::string> search(const std::string &text) const;

    // Count how many words in this trie are existing
    int count_words() const;

    // Split tags from text if existing
    std::array<std::string, 3> extract_tags(std::string text) const;

private:
    // Root pointer to first node
    TrieNode *root;
};
