#include "decoder.hpp"
#include "interface.hpp"

// =================================================================================================
// =================================================================================================

Decoder::Decoder(
    std::vector<std::string> alphabet,
    size_t max_beam_size,
    std::vector<std::vector<std::string>> vocabulary,
    std::vector<std::string> intents,
    std::vector<std::string> lm_paths,
    float lm_alpha,
    float lm_beta,
    float logit_weight,
    float token_min_logprob,
    size_t token_max_count,
    float beam_outlier_threshold,
    float unk_score_offset,
    size_t avg_token_len,
    float trie_split_offset,
    float entity_bonus,
    std::vector<float> intent_offsets,
    int blank_id)
{
    this->decoder = new DecoderInternal(
        alphabet,
        max_beam_size,
        vocabulary,
        intents,
        lm_paths,
        lm_alpha,
        lm_beta,
        logit_weight,
        token_min_logprob,
        token_max_count,
        beam_outlier_threshold,
        unk_score_offset,
        avg_token_len,
        trie_split_offset,
        entity_bonus,
        intent_offsets,
        blank_id);
}

// =================================================================================================

std::vector<BeamResult> Decoder::decode(std::vector<std::vector<float>> probs)
{
    this->decoder->decode(probs);
    return this->decoder->get_beam_results();
}

// =================================================================================================

std::string Decoder::decode_greedy_text(std::vector<std::vector<float>> probs)
{

    return this->decoder->decode_greedy_text(probs);
}

// =================================================================================================

void Decoder::reset()
{
    this->decoder->reset();
}

// =================================================================================================

std::vector<BeamResult> Decoder::decode_step(std::vector<float> probs)
{
    this->decoder->decode_step(probs);
    return this->decoder->get_beam_results();
}

// =================================================================================================

std::vector<BeamResult> Decoder::finalize()
{
    return this->decoder->finalize();
}

// =================================================================================================

Intent Decoder::extract_text2intent(const BeamResult beam_result)
{
    return this->decoder->extract_text2intent(beam_result);
}
