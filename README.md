# Slungt

Integrating SLU-tagging directly into n-gram language models.
The general idea is similar to [finstreder](https://gitlab.com/Jaco-Assistant/finstreder), but instead of calculating the shortest path in a combination of _Finite State Transducers_, a _beam search_ algorithm with additional tags is used here.

[![pipeline status](https://gitlab.com/Jaco-Assistant/slungt/badges/master/pipeline.svg)](https://gitlab.com/Jaco-Assistant/slungt/-/commits/master)
[![code style black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![code complexity](https://gitlab.com/Jaco-Assistant/slungt/-/jobs/artifacts/master/raw/badges/rcc.svg?job=analysis)](https://gitlab.com/Jaco-Assistant/slungt/-/commits/master)

<div align="center">
    <img src="media/trie.png" alt="trie embedded tagging" width="75%"/>
</div>

<br>

## Installation

```bash
docker build --progress=plain -t slungt - < dockerfile

./run_container.sh
```

<br>

## Usage

Using slungt is very simple:

```python
# Create decoder
decoder = slungt.Decoder(
    ["a", "b", " ", "yes ", ""],             # alphabet (single characters or sentencepiece)
    128,                                     # beam size
    [["ab", "ba", "[abba](band)"], ["yes"]], # list of words per intent
    ["i1", "i2"],                            # intent names
    ["i1.bin", "i2.bin"],                    # path to ngram models
)

# Decode probabilities
results = decoder.decode(
  [
    [0.9, 0.0, 0.0, 0.1, 0.0],
    [0.0, 0.9, 0.0, 0.1, 0.0],
    [0.0, 0.0, 1.0, 0.0, 0.0],
    ...
  ]  # ctc tokens with shape: (timesteps, alphabet)
)

# Use results (text, intent, log score)
print(results[0])
# ('ab [abba](band) ba', i1, -1.6732)
```

See [test_interface.cpp](tests/test_interface.cpp) or [test_interface.py](tests/test_interface.py) for more detailled usage examples. \
The interface description can be found at [interface.hpp](slungt/interface.hpp).

<br>

## Debugging

- Create testing data files:

  ```bash
  cd /slungt/tests/ && ./create_test_inputs.sh && cd ..
  ```

- Test trie functionality:

  ```bash
  cd /slungt/tests/
  g++ ../slungt/*trie*.cpp test_trie.cpp -std=c++2a -Wall -o test_trie.bin
  ./test_trie.bin

  cd /slungt/swig/ && make all && cd /slungt/tests/ && python3 test_trie.py
  ```

- Test language model bindings:

  ```bash
  cd /slungt/tests/
  g++ test_langmodel_minimal.cpp -Wall -DKENLM_MAX_ORDER=6 -I/kenlm/ -L/kenlm/build/lib/ -lkenlm -lkenlm_util -lz -llzma -lbz2 -o test_langmodel_minimal.bin && ./test_langmodel_minimal.bin
  python3 test_langmodel.py
  g++ ../slungt/langmodel.cpp test_langmodel.cpp -Wall -DKENLM_MAX_ORDER=6 -I/kenlm/ -L/kenlm/build/lib/ -lkenlm -lkenlm_util -lz -llzma -lbz2 -o test_langmodel.bin && ./test_langmodel.bin
  ```

- Test slungt interface:

  ```bash
  cd /slungt/tests/
  g++ ../slungt/*.cpp test_interface.cpp -std=c++2a -O3 -Wall -Werror -fPIC -flto -DKENLM_MAX_ORDER=6 -I/kenlm/ -L/kenlm/build/lib/ -lkenlm -lkenlm_util -lz -llzma -lbz2 -o test_interface.bin && ./test_interface.bin

  cd /slungt/swig/ && make all && cd /slungt/tests/ && python3 test_interface.py
  ```

<br>

## Testing

Build container with testing tools:

```bash
docker build -f tests/dockerfile -t testing_slungt .

docker run --network host --rm \
  --volume `pwd`/:/slungt/ \
  -it testing_slungt
```

For syntax tests, check out the steps in the [gitlab-ci](.gitlab-ci.yml#L75) file.
